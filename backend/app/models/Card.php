<?php

declare(strict_types=1);

namespace app\models;

use app\db\DbModel;

class Card extends DbModel
{
    public int $id = 0;
    public string $title = '';
    public string $description = '';
    public int $status = 0;
    public string $start = '';
    public string $end = '';
    public int $column_id = 0;
    public string $attach_url = '';

    public static function tableName(): string
    {
        return 'Cards';
    }

    public function attributes(): array
    {
        return ['title', 'description', 'status', 'start', 'end', 'attach_url', 'column_id'];
    }

    public function rules(): array
    {
        return [
            'title' => [
                self::RULE_REQUIRED,
                self::LETTERS_SPACES_AND_NUMBERS
            ],
            'status' => [self::RULE_BOOLEAN],
            'column_id' => [
                self::RULE_REQUIRED,
                self::RULE_INTEGER
            ]
        ];
    }
}
