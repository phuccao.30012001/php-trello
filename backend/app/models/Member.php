<?php

declare(strict_types=1);

namespace app\models;

use app\db\DbModel;
use PDO;

class Member extends DbModel
{
    public int $user_id = 0;
    public int $card_id = 0;

    public function attributes(): array
    {
        return ['user_id', 'card_id'];
    }

    public function rules(): array
    {
        return [
            'user_id' => [
                self::RULE_INTEGER,
                self::RULE_REQUIRED
            ],
            'card_id' => [
                self::RULE_INTEGER,
                self::RULE_REQUIRED
            ]
        ];
    }

    public function getListAvailableMember(int $card_id, string $table_join_name)
    {
        $table_name = $this->tableName();
        $select = "$table_name.card_id, $table_name.user_id, $table_join_name.id, $table_join_name.fullname";
        $sql_string = "SELECT $select 
                        FROM $table_name RIGHT JOIN $table_join_name 
                            ON $table_name.user_id = $table_join_name.id AND $table_name.card_id = $card_id 
                        WHERE $table_name.card_id IS NULL";
        $statement = self::prepare($sql_string);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function tableName(): string
    {
        return 'Members';
    }
}
