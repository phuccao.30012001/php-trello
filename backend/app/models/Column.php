<?php

declare(strict_types=1);

namespace app\models;

use app\db\DbModel;

class Column extends DbModel
{
    public int $id = 0;
    public string $title = '';

    public static function tableName(): string
    {
        return 'Columns';
    }

    public function attributes(): array
    {
        return ['title'];
    }

    public function rules(): array
    {
        return [
            'title' => [
                self::RULE_REQUIRED,
                self::LETTERS_SPACES_AND_NUMBERS
            ]
        ];
    }
}
