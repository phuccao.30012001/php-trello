<?php

declare(strict_types=1);

namespace app\models;

use app\db\DbModel;

class User extends DbModel
{
    public int $id = 0;
    public string $fullname = '';
    public string $password = '';
    public string $email = '';

    public static function tableName(): string
    {
        return 'Users';
    }

    public function attributes(): array
    {
        return ['fullname', 'password', 'email'];
    }

    public function rules(): array
    {
        return [
            'email' => [
                self::RULE_REQUIRED,
                self::RULE_EMAIL,
                self::RULE_UNIQUE,
                'class' => self::class
            ],
            'fullname' => [
                self::RULE_REQUIRED,
                self::LETTERS_AND_SPACES
            ],
            'password' => [
                self::RULE_REQUIRED,
                self::RULE_PASSWORD
            ],
        ];
    }


    public function save()
    {
        //thực hiện mã hóa mật khẩu
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);

        return parent::save();
    }
}
