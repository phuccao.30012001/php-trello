<?php

namespace app\models;

use app\db\DbModel;

class Comment extends DbModel
{
    public int $id = 0;
    public string $content = '';
    public int $card_id = 0;
    public int $user_id = 0;

    public static function tableName(): string
    {
        return 'Comments';
    }

    public function attributes(): array
    {
        return ['content', 'card_id', 'user_id'];
    }

    public function rules(): array
    {
        return [
            'content' => [
                self::RULE_REQUIRED,
                self::LETTERS_SPACES_AND_NUMBERS
            ],
            'card_id' => [
                self::RULE_REQUIRED,
                self::RULE_INTEGER
            ],
            'user_id' => [
                self::RULE_REQUIRED,
                self::RULE_INTEGER
            ]
        ];
    }

}