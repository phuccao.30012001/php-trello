<?php

declare(strict_types=1);

namespace app\models;

use app\db\DbModel;

class CheckList extends DbModel
{
    public int $id = 0;
    public string $content = '';
    public int $card_id = 0;
    public int $status = 0;

    public static function tableName(): string
    {
        return 'Checklists';
    }

    public function attributes(): array
    {
        return ['content', 'card_id', 'status'];
    }

    public function rules(): array
    {
        return [
            'content' => [
                self::RULE_REQUIRED,
                self::LETTERS_SPACES_AND_NUMBERS
            ],
            'card_id' => [
                self::RULE_REQUIRED,
                self::RULE_INTEGER
            ],
            'status' => [self::RULE_BOOLEAN]
        ];
    }
}
