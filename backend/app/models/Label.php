<?php

declare(strict_types=1);

namespace app\models;

use app\db\DbModel;

class Label extends DbModel
{
    public int $id = 0;
    public string $title = '';
    public int $card_id = 0;
    public string $color = '';

    public static function tableName(): string
    {
        return 'Labels';
    }

    public function attributes(): array
    {
        return ['title', 'card_id', 'color'];
    }

    public function rules(): array
    {
        return [
            'title' => [
                self::RULE_REQUIRED,
                self::LETTERS_SPACES_AND_NUMBERS
            ],
            'color' => [self::RULE_REQUIRED],
            'card_id' => [
                self::RULE_REQUIRED,
                self::RULE_INTEGER
            ]
        ];
    }
}
