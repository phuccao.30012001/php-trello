<?php

declare(strict_types=1);

namespace app\controllers;

use app\core\Request;
use app\core\Response;
use app\models\User;


class UserController extends ApiController
{
    public const EXPIRED_HOUR = 60 * 60;
    public const EXPIRED_DAY = 60 * 60 * 24;

    public function getProfile(Request $req, Response $res)
    {
        $user_model = new User();
        $payload_id = $this->acceptJwtVisit($req, $res);
        if (!is_int($payload_id)) {
            return null;
        }

        $data_profile = $user_model->findOne(['id' => $payload_id]);
        if (!$data_profile) {
            return $this->respondNotFound($res, 'Không thể tìm thấy thông tin người dùng');
        }
        //xóa bỏ field password created_at errors
        unset($data_profile->password, $data_profile->created_at, $data_profile->errors, $data_profile->email);
        return $this->respondWithData($res, $data_profile, 'Tìm thông tin người dùng thành công');
    }

    public function register(Request $req, Response $res)
    {
        $user_model = new User();

        $req_payload = $req->getBody();


        $user_model->loadData($req_payload);

        if (!$this->validate($user_model)) {
            return $this->respondError($res, $user_model->errors[0]);
        }

        if ($user_model->findOne(['email' => $req_payload['email']])) {
            return $this->respondError($res, 'Người dùng đã tồn tại');
        }


        if (!$user_model->save()) {
            return $this->respondError($res, 'Không thể tạo người dùng');
        }
        return $this->respondCreated($res, 'Đăng kí người dùng thành công');
    }

    public function login(Request $req, Response $res)
    {
        $user_model = new User();
        $req_payload = $req->getBody();
        $user = $user_model->findOne(['email' => $req_payload['email']]);
        if (!$user) {
            return $this->respondUnauthorized($res, 'Email, mật khẩu không chính xác');
        }

        if (!password_verify($req_payload['password'], $user->password)) {
            return $this->respondUnauthorized($res, 'Email, mật khẩu không chính xác');
        }

        $payload_token = [
            'id' => $user->id,
            'exp' => time() + self::EXPIRED_HOUR,  //1h
            'ip' => $req->getIp()
        ];

        $payload_refresh = [
            'id' => $user->id,
            'exp' => time() + self::EXPIRED_DAY, //24h
            'ip' => $req->getIp()
        ];

        $token = JwtController::generateToken($payload_token);
        $refresh = JwtController::generateRefresh($payload_refresh);

        return $this->respondWithData($res, ['accessToken' => $token, 'refreshToken' => $refresh], 'Login success');
    }

    public function updateProfile(Request $req, Response $res)
    {
        $payload_id = $this->acceptJwtVisit($req, $res);

        if (!is_int($payload_id)) {
            return null;
        }

        $user_model = new User();
        $req_payload = $req->getBody();

        $user_model->loadData($req_payload);

        unset($user_model->password, $user_model->email);

        $user_find = $user_model->findOne(['id' => $payload_id]);
        if (!$user_find) {
            return $this->respondNotFound($res, 'Can\'t find user');
        }

        if (!$user_model->update($payload_id)) {
            return $this->respondError($res, 'Không thể cập nhật thông tin người dùng');
        }
        return $this->respondSuccess($res, "Cập nhật thông tin người dùng thành công");
    }
}
