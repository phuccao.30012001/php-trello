<?php

declare(strict_types=1);

namespace app\controllers;

use app\core\Request;
use app\core\Response;
use app\models\Column;

class ColumnController extends ApiController
{
    public function addColumn(Request $req, Response $res)
    {
        $user_id = $this->acceptJwtVisit($req, $res);

        if (!is_int($user_id)) {
            return null;
        }

        $payload_req = $req->getBody();
        $column_model = new Column();
        $column_model->loadData($payload_req);
        if (!$this->validate($column_model)) {
            return $this->respondError($res, $column_model->errors[0]);
        }
        if (!$column_model->save()) {
            return $this->respondError($res, 'Không thể tạo mới cột');
        }

        return $this->respondSuccess($res, 'Tạo mới cột thành công');
    }

    public function getList(Request $req, Response $res)
    {
        $user_id = $this->acceptJwtVisit($req, $res);

        if (!is_int($user_id)) {
            return null;
        }

        $column_model = new Column();

        $list_column = $column_model->findAll('id');

        return $this->respondWithData($res, $list_column, 'Tìm thấy danh sách cột thành công');
    }

    public function deleteColumn(Request $req, Response $res)
    {
        $user_id = $this->acceptJwtVisit($req, $res);

        if (!is_int($user_id)) {
            return null;
        }

        $id = $req->getRouteParam('id');

        $column_model = new Column();

        if (!$column_model->delete($id)) {
            return $this->respondError($res, 'Không thể xóa cột');
        }

        return $this->respondSuccess($res, 'Xóa cột thành công');
    }

    public function updateColumn(Request $req, Response $res)
    {
        $user_id = $this->acceptJwtVisit($req, $res);

        if (!is_int($user_id)) {
            return null;
        }

        $id = $req->getRouteParam('id');
        $payload_req = $req->getBody();

        settype($payload_req['id'], "integer");
        $column_model = new Column();
        $column_model->loadData($payload_req);

        if (!$this->validate($column_model)) {
            return $this->respondError($res, $column_model->errors[0]);
        }
        unset($column_model->id);


        $column_model->update($id);

        return $this->respondSuccess($res, 'Cập nhật cột thành công');
    }
}
