<?php

declare(strict_types=1);

namespace app\controllers;

use app\core\Request;
use app\core\Response;
use app\models\Card;
use DateTime;


class CardController extends ApiController
{
    public function addCard(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $card_model = new Card();
        $payload_req = $request->getBody();

        $card_model->loadData($payload_req);
        $date_time = new DateTime();
        $card_model->start = $date_time->format('Y-m-d');
        $card_model->end = $date_time->format('Y-m-d');
        if (!$this->validate($card_model)) {
            return $this->respondError($response, $card_model->errors[0]);
        }

        if (!$card_model->save()) {
            return $this->respondError($response, 'Không thể tạo mới thẻ');
        }

        return $this->respondSuccess($response, 'Tạo mới thẻ thành công');
    }

    public function updateCard(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $card_model = new Card();

        $payload_req = $request->getBody();
        $card_model->loadData($payload_req, 'attach_url');

        $id = $request->getRouteParam('id');
        if (!$card_model->findOne(['id' => $id])) {
            return $this->respondError($response, 'Không tìm thấy thẻ');
        }


        if (!$this->validate($card_model)) {
            return $this->respondError($response, $card_model->errors[0]);
        }

        unset($card_model->id, $card_model->column_id);


        if (!$card_model->update($id)) {
            return $this->respondError($response, 'Không thể cập nhật thẻ');
        }

        return $this->respondSuccess($response, 'Cập nhật thẻ thành công');
    }

    public function deleteCard(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $card_model = new Card();

        $id = $request->getRouteParam('id');
        if (!$card_model->findOne(['id' => $id])) {
            return $this->respondError($response, 'Không tìm thấy thẻ');
        }
        if (!$card_model->delete($id)) {
            return $this->respondError($response, 'Không thể xóa thẻ');
        }

        return $this->respondSuccess($response, 'Xóa thẻ thành công');
    }

    public function getListByColumn(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $card_model = new Card();

        $id = (int)$request->getRouteParam('id');

        $list_card = $card_model->findById('column_id', $id);

        if (!$list_card) {
            return $this->respondError($response, 'Không thể tìm thấy danh sách thẻ');
        }
        return $this->respondWithData($response, $list_card, 'Tìm thấy dánh sách thẻ thành công');
    }
}
