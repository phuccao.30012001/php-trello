<?php

declare(strict_types=1);

namespace app\controllers;

use app\core\Request;
use app\core\Response;
use app\models\CheckList;

class CheckListController extends ApiController
{
    public function addCheckList(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $checklist_model = new CheckList();

        $payload_req = $request->getBody();
        $checklist_model->loadData($payload_req);

        if (!$this->validate($checklist_model)) {
            return $this->respondError($response, $checklist_model->errors[0]);
        }

        if (!$checklist_model->save()) {
            return $this->respondError($response, 'Không thể tạo mới công việc');
        }
        return $this->respondSuccess($response, 'Tạo mới công việc thành công');
    }

    public function deleteCheckList(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $checklist_model = new CheckList();

        $checklist_id = (int)$request->getRouteParam('id');

        if (!$checklist_model->delete($checklist_id)) {
            return $this->respondError($response, 'Không thể xóa công việc');
        }

        return $this->respondSuccess($response, 'Xóa công việc thành công');
    }

    public function updateCheckList(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $checklist_model = new CheckList();

        $payload_req = $request->getBody();
        $checklist_model->loadData($payload_req);

        $card_id = (int)$request->getRouteParam('id');
        if (!$this->validate($checklist_model)) {
            return $this->respondError($response, $checklist_model->errors[0]);
        }
        unset($checklist_model->card_id, $checklist_model->id);

        if (!$checklist_model->update($card_id)) {
            return $this->respondError($response, 'Không thể cập nhật công việc');
        }
        return $this->respondSuccess($response, 'Cập nhật công việc thành công');
    }

    public function getCheckList(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $checklist_model = new CheckList();

        $card_id = $request->getRouteParam('id');

        $list_checklist = $checklist_model->findById('card_id', $card_id);

        if (!$list_checklist) {
            return $this->respondError($response, 'Không tìm thấy công việc');
        }
        return $this->respondWithData($response, $list_checklist, 'Tìm thấy danh sách công việc thành công');
    }
}
