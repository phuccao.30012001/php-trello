<?php

declare(strict_types=1);

namespace app\controllers;


require_once dirname(__DIR__) . '/vendor/autoload.php';

use app\core\Request;
use app\core\Response;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class JwtController
{
    public static function generateToken(array $payload): string
    {
        $key = $_ENV['JWT_SECRET'];
        return JWT::encode($payload, $key, 'HS256');
    }

    public static function generateRefresh(array $payload): string
    {
        $key = $_ENV['JWT_REFRESH'];
        return JWT::encode($payload, $key, 'HS256');
    }

//    public static function verifyRefresh(string $token)
//    {
//        $key = $_ENV['JWT_REFRESH'];
//        try {
//            return JWT::decode($token, $key, ['HS256']);
//        } catch (Exception $e) {
//            return false;
//        }
//    }

    public static function visit(ApiController $apiController, Request $request, Response $response)
    {
        $payload_token = self::authorization($request);
        if (!$payload_token) {
            return $apiController->respondUnauthorized($response, 'Chưa xác thực');
        }
        return $payload_token->id;
    }

    public static function authorization(Request $request)
    {
        $decoded = self::verifyToken((string)$request->getHeader('Authorization'));
        if (!$decoded) {
            return false;
        }
        if ($decoded->ip !== $request->getIp()) {
            return false;
        }
        return $decoded;
    }

    public static function verifyToken(string $token)
    {
        $key = $_ENV['JWT_SECRET'];
        $token = str_replace('Bearer ', '', $token);
        try {
            return JWT::decode($token, new Key($key, 'HS256'));
        } catch (Exception $e) {
            return false;
        }
    }
}
