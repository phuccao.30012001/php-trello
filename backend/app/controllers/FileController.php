<?php

namespace app\controllers;

use app\core\Cloudinary;
use app\core\Request;
use app\core\Response;

class FileController extends ApiController
{
    public function uploadFile(Request $request, Response $response)
    {
        $file = $request->getFile();

        $upload_file = new Cloudinary();
        $response_upload_file = $upload_file->uploadFile($file['file']['tmp_name']);
        $data_response = [
            'secure_url' => $response_upload_file['secure_url'],
            'public_id' => $response_upload_file['public_id']
        ];
        return $this->respondWithData($response, $data_response, 'Tải file lên thành công');
    }
}
