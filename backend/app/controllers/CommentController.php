<?php

namespace app\controllers;

use app\core\Request;
use app\core\Response;
use app\models\Comment;
use app\models\User;

class CommentController extends ApiController
{
    public function addComment(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $comment_model = new Comment();
        $payload_req = $request->getBody();

        $comment_model->loadData($payload_req);

        if (!$this->validate($comment_model)) {
            return $this->respondError($response, $comment_model->errors[0]);
        }

        if (!$comment_model->save()) {
            return $this->respondError($response, 'Không thể tạo mới bình luận');
        }
        return $this->respondSuccess($response, 'Tạo mới bình luận thành công');

    }

    public function getListComment(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }


        $card_id = (int)$request->getRouteParam('id');
        $comment_model = new Comment();

        $join_table_name = User::tableName();
        $select_field = [Comment::tableName() . '.id', 'fullname', 'content'];

        $list_comment = $comment_model->findInnerJoin($select_field, ['card_id' => $card_id], 'user_id', 'id', $join_table_name);
        if (!$list_comment) {
            return $this->respondError($response, 'Không thể tìm thấy bình luận');
        }
        return $this->respondWithData($response, $list_comment, 'Tìm thấy bình luận thành công');
    }
}