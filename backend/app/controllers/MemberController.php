<?php

declare(strict_types=1);

namespace app\controllers;

use app\core\Request;
use app\core\Response;
use app\models\Member;
use app\models\User;

class MemberController extends ApiController
{
    public function getListMember(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $member_model = new Member();
        $card_id = $request->getRouteParam('id');
        $join_table_name = User::tableName();
        $select_field = [Member::tableName() . '.id', 'user_id', 'card_id', 'fullname'];

        $list_member = $member_model->findInnerJoin($select_field, ['card_id' => $card_id], 'user_id', 'id', $join_table_name);
        if (!$list_member) {
            return $this->respondError($response, 'Không thể tìm thấy thành viên');
        }
        return $this->respondWithData($response, $list_member, 'Tìm thấy thành viên thành công');
    }

    public function getListAvailableMember(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $member_model = new Member();
        $card_id = (int)$request->getRouteParam('id');
        $join_table_name = User::tableName();
        $list_member_available = $member_model->getListAvailableMember($card_id, $join_table_name);
        if (!$list_member_available) {
            return $this->respondError($response, 'Không tìm thấy thành viên còn trống');
        }
        return $this->respondWithData($response, $list_member_available, 'Tìm thấy danh sách thành viên thành công');
    }

    public function addMember(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $member_model = new Member();
        $payload_req = $request->getBody();
        if (!isset($payload_req['members'])) {
            $this->respondError($response, 'Không thể thêm thành viên');
        }
        $list_member = $payload_req['members'];
        foreach ($list_member as $member) {
            if (isset($member['user_id']) && isset($member['card_id'])) {
                $user_id = (int)$member['user_id'];
                $card_id = (int)$member['card_id'];
                if (!$member_model->findOne(['user_id' => $user_id, 'card_id' => $card_id])) {
                    $member_model->loadData(['user_id' => $user_id, 'card_id' => $card_id]);
                    $member_model->save();
                }
            }
        }
        return $this->respondSuccess($response, 'Thêm danh sách thành viên thành công');
    }

    public function deleteMember(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $member_model = new Member();
        $member_id = $request->getRouteParam('id');
        if (!$member_model->delete($member_id)) {
            return $this->respondError($response, 'Không thể xóa thành viên');
        }
        return $this->respondSuccess($response, 'Xóa thành viên thành công');
    }
}
