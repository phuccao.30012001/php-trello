<?php

declare(strict_types=1);

namespace app\controllers;

use app\core\Request;
use app\core\Response;
use app\models\Label;

class LabelController extends ApiController
{
    public function addLabel(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $label_model = new Label();
        $payload_req = $request->getBody();
        $label_model->loadData($payload_req);

        if (!$this->validate($label_model)) {
            return $this->respondError($response, $label_model->errors[0]);
        }

        if (!$label_model->save()) {
            return $this->respondError($response, 'Không thể tạo mới nhãn');
        }
        return $this->respondSuccess($response, 'Tạo mới nhãn thành công');
    }

    public function updateLabel(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $label_model = new Label();
        $id = $request->getRouteParam('id');
        $payload_req = $request->getBody();
        $label_model->loadData($payload_req);
        if (!$this->validate($label_model)) {
            return $this->respondError($response, $label_model->errors[0]);
        }

        if (!$label_model->update($id)) {
            return $this->respondError($response, 'Không thể cập nhật nhãn');
        }
        return $this->respondSuccess($response, 'Cập nhật nhãn thành công');
    }

    public function deleteLabel(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $id = $request->getRouteParam('id');
        $label_model = new Label();

        if (!$label_model->delete($id)) {
            return $this->respondError($response, 'Không thể xóa nhãn');
        }

        return $this->respondSuccess($response, 'Xóa nhãn thành công');
    }

    public function getListLabel(Request $request, Response $response)
    {
        $user_id = $this->acceptJwtVisit($request, $response);

        if (!is_int($user_id)) {
            return null;
        }

        $card_id = $request->getRouteParam('id');
        $label_model = new Label();
        $list_label = $label_model->findById('card_id', $card_id);

        if (!$list_label) {
            return $this->respondError($response, 'không thể tìm thấy danh sách các nhãn');
        }

        return $this->respondWithData($response, $list_label, 'Tìm thấy danh sách nhãn thành công');
    }
}
