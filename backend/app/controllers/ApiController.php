<?php

declare(strict_types=1);

namespace app\controllers;

use app\core\Application;
use app\core\Model;
use app\core\Request;
use app\core\Response;

class ApiController
{
    const STATUS = 'status';
    const SUCCESS_STATUS_CODE = 200;
    const CREATED_STATUS_CODE = 201;
    const ERROR_STATUS_CODE = 400;
    const UNAUTHORIZED_STATUS_CODE = 401;
    const NOT_FOUND_STATUS_CODE = 404;

    public function respondWithData(Response $response, $data, string $message)
    {
        $response->setStatusCode(self::SUCCESS_STATUS_CODE);
        $response->setData([self::STATUS => true, 'message' => $message, 'data' => $data]);

        return $response->json($response->data);
    }

    public function respondError(Response $response, string $error)
    {
        $response->setStatusCode(self::ERROR_STATUS_CODE);
        $response->setData([self::STATUS => false, 'message' => $error]);

        return $response->json($response->data);
    }

    public function respondSuccess(Response $response, string $success)
    {
        $response->setStatusCode(self::SUCCESS_STATUS_CODE);
        $response->setData([self::STATUS => true, 'message' => $success]);

        return $response->json($response->data);
    }

    public function respondNotFound(Response $response, string $error)
    {
        $response->setStatusCode(self::NOT_FOUND_STATUS_CODE);
        $response->setData([self::STATUS => false, 'message' => $error]);

        return $response->json($response->data);
    }

    public function respondCreated(Response $response, string $success)
    {
        $response->setStatusCode(self::CREATED_STATUS_CODE);
        $response->setData([self::STATUS => true, 'message' => $success]);

        return $response->json($response->data);
    }

    public function respondUnauthorized(Response $response, string $error)
    {
        $response->setStatusCode(self::UNAUTHORIZED_STATUS_CODE);
        $response->setData([self::STATUS => false, 'message' => $error]);

        return $response->json($response->data);
    }

    public function validate(Model $model): bool
    {
        $rule = $model->rules();
        foreach ($rule as $attribute => $rules) {
            $value = $model->{$attribute};
            foreach ($rules as $rule) {
                $rule_name = $rule;
                $this->validateRequired($model, $rule_name, $value, $attribute);
                $this->validateBoolean($model, $rule_name, $value, $attribute);
                $this->validateEmail($model, $rule_name, $value, $attribute);
                $this->validateInteger($model, $rule_name, $value, $attribute);
                $this->validateLetterSpace($model, $rule_name, $value, $attribute);
                $this->validateLetterSpaceNumber($model, $rule_name, $value, $attribute);
                $this->validatePassword($model, $rule_name, $value, $attribute);
                $this->validateUnique($model, $rule_name, $value, $attribute, $rules);
            }

        }
        return empty($model->errors);
    }

    protected function validateRequired(Model $model, string $rule_name, $value, $attribute): void
    {
        if ($rule_name === Model::RULE_REQUIRED && !isset($model->{$attribute})) {
            $model->addErrorByRule($attribute, Model::RULE_REQUIRED);
        }
    }

    protected function validateBoolean(Model $model, string $rule_name, $value, $attribute): void
    {
        if ($rule_name === Model::RULE_BOOLEAN && !($value === 1 || $value === 0)) {
            $model->addErrorByRule($attribute, Model::RULE_BOOLEAN);
        }
    }

    protected function validateEmail(Model $model, string $rule_name, $value, $attribute): void
    {
        if ($rule_name === Model::RULE_EMAIL && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $model->addErrorByRule($attribute, Model::RULE_EMAIL);
        }
    }

    protected function validateInteger(Model $model, string $rule_name, $value, $attribute): void
    {
        if ($rule_name === Model::RULE_INTEGER && !is_int($value)) {
            $model->addErrorByRule($attribute, Model::RULE_INTEGER);
        }
    }

    protected function validateLetterSpace(Model $model, string $rule_name, $value, $attribute): void
    {
        if ($rule_name === Model::LETTERS_AND_SPACES && !preg_match('/^[a-zA-Z ]*$/', $value)) {
            $model->addErrorByRule($attribute, Model::LETTERS_AND_SPACES);
        }
    }

    protected function validateLetterSpaceNumber(Model $model, string $rule_name, $value, $attribute): void
    {
        if ($rule_name === Model::LETTERS_SPACES_AND_NUMBERS && !preg_match('/^[a-zA-Z0-9 ]+$/', $value)) {
            $model->addErrorByRule($attribute, Model::LETTERS_SPACES_AND_NUMBERS);
        }
    }

    protected function validatePassword(Model $model, string $rule_name, $value, $attribute): void
    {
        if ($rule_name === Model::RULE_PASSWORD && !preg_match('/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/', $value)) {
            $model->addErrorByRule($attribute, Model::RULE_PASSWORD);
        }
    }


    public function validateUnique(Model $model, string $rule_name, $value, $attribute, $rules): void
    {
        if ($rule_name === Model::RULE_UNIQUE) {
            $class_name = $rules['class'];
            $table_name = $class_name::tableName();
            $db = Application::$app->db;
            $statement = $db->prepare("SELECT * FROM $table_name WHERE $attribute = :$attribute");
            $statement->bindValue(":$attribute", $value);
            $statement->execute();
            $record = $statement->fetchObject();
            if ($record) {
                $model->addErrorByRule($attribute, Model::RULE_UNIQUE, ['filed' => $attribute]);
            }
        }
    }

    public function acceptJwtVisit(Request $request, Response $response)
    {
        return JwtController::visit($this, $request, $response);
    }
}
