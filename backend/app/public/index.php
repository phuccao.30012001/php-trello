<?php

declare(strict_types=1);

use app\core\Application;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin,Cache-Control, Pragma, Content-Type, Authorization, X-Requested-With');
header('Content-Type: application/x-www-form-urlencoded, multipart/form-data, application/json');
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    return 204;
}

// Nạp các dependencie trong composer vào php thông qua autoload.php
require_once __DIR__ . "/../vendor/autoload.php";
//Đọc file .env
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();
$config = [
    'db' => [
        'host' => $_ENV['DB_HOST'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
        'dbname' => $_ENV['DB_NAME']
    ]
];

$app = new Application($config);
//Kết nối tới db
$app->db->connect();

//test
$app->router->get('/api', function () {
    echo 'Hello World!';
});

// api user
(new app\routers\UserRouter($app))->router();

// api column
(new app\routers\ColumnRouter($app))->router();
// api card
(new app\routers\CardRouter($app))->router();
// api member
(new app\routers\MemberRouter($app))->router();

// api checklist
(new app\routers\CheckListRouter($app))->router();

// api label
(new app\routers\LabelRouter($app))->router();

// api file
(new app\routers\FileRouter($app))->router();

// api comment
(new app\routers\CommentRouter($app))->router();


$app->run();
