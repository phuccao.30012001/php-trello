<?php

declare(strict_types=1);

namespace app\core;

class Request
{
    private array $routeParams = [];

    public function getPath(): string
    {
        $path = $_SERVER['REQUEST_URI'] ?? '/';
        $position = strpos($path, '?');
        if ($position === false) {
            return $path;
        }

        return substr($path, 0, $position);
    }

    public function getMethod(): string
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    public function getBody()
    {
        // Read the input stream
        $body = file_get_contents("php://input");

        // Decode the JSON object
        return json_decode($body, true);
    }

    public function getFile(): array
    {
        $files = [];
        foreach ($_FILES as $key => $value) {
            $files[$key] = $value;
        }
        return $files;
    }

    public function getHeader(string $name)
    {
        $headers = $this->getHeaders();
        return $headers[$name] ?? null;
    }

    public function getHeaders()
    {
        return getallheaders();
    }

    public function setRouteParams(array $routeParams): void
    {
        $this->routeParams = $routeParams;
    }


    public function getRouteParam(string $param, $default = null)
    {
        return $this->routeParams[$param] ?? $default;
    }

    public function getIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}
