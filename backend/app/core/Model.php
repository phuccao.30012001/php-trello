<?php

declare(strict_types=1);

namespace app\core;

class Model
{
    public const RULE_REQUIRED = 'required';
    public const RULE_EMAIL = 'email';
    public const RULE_UNIQUE = 'unique';
    public const RULE_INTEGER = 'integer';
    public const LETTERS_AND_SPACES = 'lettersAndSpaces';
    public const LETTERS_SPACES_AND_NUMBERS = 'lettersSpacesAndNumbers';
    public const RULE_PASSWORD = 'password';
    public const RULE_BOOLEAN = 'boolean';
    public const RULE_URL = 'url';

    public array $errors = [];

    public function attributes(): array
    {
        return [];
    }

    public function loadData($data, string $except = ''): void
    {
        foreach ($data as $key => $value) {
            if ((property_exists($this, $key) && isset($this->{$key}) && $value) || $key === $except) {
                $this->{$key} = $value;
            }
        }
    }

    public function rules(): array
    {
        return [];
    }

    public function addErrorByRule(string $attribute, string $rule, $params = []): void
    {
        $params['field'] ??= $attribute;
        $error_message = $this->errorMessage($rule);
        foreach ($params as $key => $value) {
            $error_message = str_replace("{{$key}}", $value, $error_message);
        }
        $this->errors[] = $error_message;
    }

    public function errorMessage(string $rule): string
    {
        $error_messages = $this->errorMessages();
        return $error_messages[$rule] ?? '';
    }

    public function errorMessages(): array
    {
        return [
            self::RULE_REQUIRED => 'Trường {field} bắt buộc',
            self::RULE_EMAIL => 'Trường {field} phải có định dạng email',
            self::RULE_INTEGER => 'Trường {field} phải là số nguyên',
            self::LETTERS_AND_SPACES => 'Trường {field} chỉ bao gồm chữ cái và khoảng cách',
            self::LETTERS_SPACES_AND_NUMBERS => 'Trường {field} chỉ bao gồm chữ cái, chữ số và khoảng cách',
            self::RULE_PASSWORD => 'Mật khẩu có ít nhất 8 kí tự. Ít nhất 1 in hoa, 1 in thường, 1 số và 1 kí tự đặc biệt.',
            self::RULE_UNIQUE => 'Đã tồn tại {field}',
            self::RULE_BOOLEAN => 'Trường {field} phải là 0,1'
        ];
    }
}
