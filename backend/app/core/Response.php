<?php

declare(strict_types=1);

namespace app\core;

class Response
{
    public array $data;

    public function setStatusCode(int $code): void
    {
        http_response_code($code);
    }

    public function json(array $data): void
    {
        header('Content-Type: application/json');
        echo json_encode($data, JSON_UNESCAPED_UNICODE);
    }


    public function setData(array $data): void
    {
        $this->data = $data;
    }
}
