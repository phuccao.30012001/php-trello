<?php

namespace app\core;

require_once dirname(__DIR__) . '/vendor/autoload.php';

use Cloudinary\Api\Exception\ApiError;
use Cloudinary\Api\Upload\UploadApi;
use Cloudinary\Configuration\Configuration;

class Cloudinary
{
    public function __construct()
    {
        Configuration::instance([
            'cloud' => [
                'cloud_name' => $_ENV['CLOUDINARY_NAME'],
                'api_key' => $_ENV['CLOUDINARY_KEY'],
                'api_secret' => $_ENV['CLOUDINARY_SECRET'],
            ],
            'url' => [
                'secure' => true,
            ]
        ]);
    }

    public function uploadFile($file_data, $options = [])
    {
        try {
            return (new UploadApi())->upload($file_data, $options);
        } catch (ApiError $e) {
            // Handle the exception
            echo "Cloudinary API Error: " . $e->getMessage();
        }
    }

}
