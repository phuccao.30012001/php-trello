<?php

declare(strict_types=1);

namespace app\core;

use app\db\Database;

class Application
{
    public static Application $app;
    public Router $router;
    public Request $request;
    public Response $response;
    public Database $db;

    public function __construct(array $config)
    {
        self::$app = $this;
        $this->request = new Request();
        $this->response = new Response();
        $this->router = new Router($this->request, $this->response);
        $this->db = new Database($config['db']);
    }

    public function run(): void
    {
        $this->router->resolve();
    }
}
