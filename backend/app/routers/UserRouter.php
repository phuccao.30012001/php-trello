<?php

declare(strict_types=1);

namespace app\routers;

use app\controllers\UserController;
use app\core\Application;

class UserRouter
{
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function router(): void
    {
        $this->app->router->get('/api/user/profile', [UserController::class, 'getProfile']);
        $this->app->router->post('/api/user/register', [UserController::class, 'register']);
        $this->app->router->post('/api/user/login', [UserController::class, 'login']);
        $this->app->router->put('/api/user/update', [UserController::class, 'updateProfile']);
    }
}
