<?php

declare(strict_types=1);

namespace app\routers;

use app\controllers\MemberController;
use app\core\Application;

class MemberRouter
{
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function router(): void
    {
        $this->app->router->post('/api/member/add', [MemberController::class, 'addMember']);
        $this->app->router->get('/api/member/list/{id}', [MemberController::class, 'getListMember']);
        $this->app->router->delete('/api/member/delete/{id}', [MemberController::class, 'deleteMember']);
        $this->app->router->get('/api/member/available/{id}', [MemberController::class, 'getListAvailableMember']);
    }
}
