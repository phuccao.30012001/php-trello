<?php

declare(strict_types=1);

namespace app\routers;

use app\controllers\LabelController;
use app\core\Application;

class LabelRouter
{
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function router(): void
    {
        $this->app->router->post('/api/label/add', [LabelController::class, 'addLabel']);
        $this->app->router->put('/api/label/update/{id}', [LabelController::class, 'updateLabel']);
        $this->app->router->delete('/api/label/delete/{id}', [LabelController::class, 'deleteLabel']);
        $this->app->router->get('/api/label/list/{id}', [LabelController::class, 'getListLabel']);
    }
}
