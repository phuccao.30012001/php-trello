<?php

declare(strict_types=1);

namespace app\routers;

use app\controllers\FileController;
use app\core\Application;

class FileRouter
{
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function router(): void
    {
        $this->app->router->post('/api/file/upload', [FileController::class, 'uploadFile']);
    }
}
