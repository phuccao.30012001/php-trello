<?php

declare(strict_types=1);

namespace app\routers;

use app\controllers\ColumnController;
use app\core\Application;

class ColumnRouter
{
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function router(): void
    {
        $this->app->router->get('/api/column/list', [ColumnController::class, 'getList']);
        $this->app->router->post('/api/column/add', [ColumnController::class, 'addColumn']);
        $this->app->router->put('/api/column/update/{id}', [ColumnController::class, 'updateColumn']);
        $this->app->router->delete('/api/column/delete/{id}', [ColumnController::class, 'deleteColumn']);
    }
}
