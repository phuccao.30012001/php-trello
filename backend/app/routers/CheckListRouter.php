<?php

declare(strict_types=1);

namespace app\routers;

use app\controllers\CheckListController;
use app\core\Application;

class CheckListRouter
{
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function router(): void
    {
        $this->app->router->get('/api/checklist/list/{id}', [CheckListController::class, 'getCheckList']);
        $this->app->router->post('/api/checklist/add', [CheckListController::class, 'addCheckList']);
        $this->app->router->put('/api/checklist/update/{id}', [CheckListController::class, 'updateCheckList']);
        $this->app->router->delete('/api/checklist/delete/{id}', [CheckListController::class, 'deleteCheckList']);
    }
}