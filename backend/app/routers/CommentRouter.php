<?php

declare(strict_types=1);

namespace app\routers;

use app\controllers\CommentController;
use app\core\Application;

class CommentRouter
{
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function router(): void
    {
        $this->app->router->post('/api/comment/add', [CommentController::class, 'addComment']);
        $this->app->router->get('/api/comment/list/{id}', [CommentController::class, 'getListComment']);
    }
}
