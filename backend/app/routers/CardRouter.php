<?php

declare(strict_types=1);

namespace app\routers;

use app\controllers\CardController;
use app\core\Application;

class CardRouter
{
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function router(): void
    {
        $this->app->router->post('/api/card/add', [CardController::class, 'addCard']);
        $this->app->router->put('/api/card/update/{id}', [CardController::class, 'updateCard']);
        $this->app->router->delete('/api/card/delete/{id}', [CardController::class, 'deleteCard']);
        $this->app->router->get('/api/card/list/{id}', [CardController::class, 'getListByColumn']);
    }
}
