<?php

declare(strict_types=1);

namespace app\db;

use PDO;
use PDOStatement;

class Database
{
    public PDO $pdo;
    private string $host;
    private string $user;
    private string $password;
    private string $dbname;

    public function __construct(array $config)
    {
        $this->host = $config['host'] ?? '';
        $this->user = $config['user'] ?? '';
        $this->password = $config['password'] ?? '';
        $this->dbname = $config['dbname'] ?? '';
    }

    public function connect(): void
    {
        $dsn = "mysql:host=$this->host;dbname=$this->dbname;charset=utf8mb4";
        $this->pdo = new PDO($dsn, $this->user, $this->password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function execute(string $sql, array $params = []): PDOStatement
    {
        $statement = $this->prepare($sql);
        foreach ($params as $key => $value) {
            $statement->bindValue(":$key", $value);
        }
        $statement->execute();
        return $statement;
    }

    public function prepare(string $sql): PDOStatement
    {
        return $this->pdo->prepare($sql);
    }
}
