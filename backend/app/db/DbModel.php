<?php

declare(strict_types=1);

namespace app\db;

use app\core\Application;
use app\core\Model;
use PDO;
use PDOStatement;

abstract class DbModel extends Model
{
    public function save()
    {
        $table_name = $this->tableName();
        $attributes = $this->attributes();
        $params = array_map(fn($attr) => ":$attr", $attributes);
        $statement = self::prepare("INSERT INTO $table_name (" . implode(",", $attributes) . ") 
                VALUES (" . implode(",", $params) . ")");
        foreach ($attributes as $attribute) {
            $statement->bindValue(":$attribute", $this->{$attribute});
        }
        $statement->execute();
        $new_id = Application::$app->db->pdo->lastInsertId();
        return $this->findOne(['id' => $new_id]);
    }

    abstract public static function tableName(): string;

    public static function prepare(string $sql): PDOStatement
    {
        return Application::$app->db->prepare($sql);
    }

    public static function findOne(array $where)
    {
        $table_name = static::tableName();
        $attributes = array_keys($where);
        $sql = implode(" AND ", array_map(fn($attr) => "$attr = :$attr", $attributes));
        $statement = self::prepare("SELECT * FROM $table_name WHERE $sql");
        foreach ($where as $key => $item) {
            $statement->bindValue(":$key", $item);
        }
        $statement->execute();

        return $statement->fetchObject(static::class);
    }

    public function update($id)
    {
        $id = (int)$id;
        $table_name = $this->tableName();
        $attributes = $this->attributes();

        $set_value_string = '';

        foreach ($attributes as $attribute) {
            if (isset($this->{$attribute}) && property_exists($this, $attribute)) {
                $set_value_string .= "$attribute = :$attribute, ";
            }
        }
        $set_value_string = substr($set_value_string, 0, strlen($set_value_string) - 2);
        $sql = "UPDATE $table_name SET $set_value_string WHERE id = $id";
        $statement = self::prepare($sql);
        foreach ($attributes as $attribute) {
            if (isset($this->{$attribute}) && property_exists($this, $attribute)) {
                $statement->bindParam(":$attribute", $this->{$attribute});
            }
        }
        $statement->execute();

        return $this->findOne(['id' => $id]);
    }

    public function delete($id): bool
    {
        $table_name = $this->tableName();
        $statement = self::prepare("DELETE FROM $table_name WHERE id = :$id");
        $statement->bindValue(":$id", $id);
        $statement->execute();

        return true;
    }

    public function findAll(string $order_by)
    {
        $table_name = $this->tableName();
        $statement = self::prepare("SELECT * FROM $table_name ORDER BY $order_by");
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findById($field, $value)
    {
        $table_name = $this->tableName();
        $statement = self::prepare("SELECT * FROM $table_name WHERE $field = :$field");
        $statement->bindValue(":$field", $value);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }


    public function findInnerJoin(array $select, array $where, string $field_table1, string $field_table2, string $table_join_name)
    {
        $table_name = $this->tableName();
        $attributes = array_keys($where);
        $selected = implode(",", array_map(fn($attr) => "$attr", $select));
        $sql = implode(" AND ", array_map(fn($attr) => "$attr = :$attr", $attributes));
        $statement = self::prepare("SELECT $selected FROM $table_name INNER JOIN $table_join_name ON $table_name.$field_table1 = $table_join_name.$field_table2 WHERE $sql");
        foreach ($where as $key => $item) {
            $statement->bindValue(":$key", $item);
        }
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }
}
