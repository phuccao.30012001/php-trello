import { TokenResponseType } from "src/types/typing";

export const storeToken = ({accessToken,refreshToken}:TokenResponseType)=>{
    localStorage.setItem("accessToken",accessToken);
    localStorage.setItem("refreshToken",refreshToken);
}

export const removeToken = ()=>{
    localStorage.removeItem("accessToken");
    localStorage.removeItem("refreshToken")
}

export const getAccessToken = ()=>{
    return localStorage.getItem("accessToken");
}

export const getRefreshToken = ()=>{
    return localStorage.getItem("refreshToken");
}