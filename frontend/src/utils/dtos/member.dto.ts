import { MemberInforType } from "src/types/member";

export const memberAdapterMemberOption = (listMember: MemberInforType[])=>{
    return listMember.map((member:MemberInforType)=>{
        return {
            label: member.fullname,
            value: member.id
        }
    })
}
export const memberOptionAdapterMember = (listSelected:number[],card_id:number)=>{
    return listSelected.map((selected)=>{
        return {
            card_id,
            user_id:selected
        }
    })
}