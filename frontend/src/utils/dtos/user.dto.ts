import { UserInforType, UserResponseType } from 'src/types/user.d';
export const userInforAdapter = (user: UserResponseType)=>{
    const userInfor: UserInforType = {
        id: user.id,
        fullname: user.fullname,
    }
    return userInfor;
}