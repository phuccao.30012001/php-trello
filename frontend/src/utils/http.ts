import axios from 'axios';
import { getAccessToken } from './storeToken';

axios.interceptors.request.use((config) => {
    const token = getAccessToken();
    config.headers.Authorization = 'Bearer ' + token;
    config.headers.Accept = 'application/json;';
    config.headers['Content-Type']= 'application/json'
    return config;
});
