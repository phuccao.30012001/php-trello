import { notification } from 'antd';

const colors = {
    "Thành công":"green",
    "Phản hồi":"yellow",
    "Lỗi":"Red"
}
const openNotification = (
    type: 'Thành công'|'Lỗi'|'Phản hồi',
    value: string,
) => {
    const key = `open${Date.now()}`;
    notification.open({
        message: type,
        description: value,
        key,
        duration:2,
        style:{
            backgroundColor: colors[type]
        }
    });
};
export default openNotification;
