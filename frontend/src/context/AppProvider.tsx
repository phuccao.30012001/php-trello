import React, { useEffect, useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { getProfile } from 'src/apis/auth.request';
import { IAppContext } from 'src/types/AppProvider';
import { UserInforType } from 'src/types/user';

type AppContextProps = {
    children: React.ReactNode;
};

const initialContext: IAppContext = {
    user: undefined,
    getInforUser: async () => {},
};

export const AppContext = React.createContext<IAppContext>(initialContext);

export default function AppProvider({ children }: AppContextProps) {
    const [user, setUser] = useState<UserInforType>();
    const router = useNavigate();
    const pathname = useLocation().pathname;
    const getInforUser = async () => {
        try {
            const result = await getProfile();
            if (!result.data.status) {
                router('/login');
            } else {
                setUser(result.data.data);
                if (pathname !== '/manage' && pathname !== '/profile')
                    router('/manage');
            }
        } catch (error) {
            router('/login');
        }
    };
    const value: IAppContext = {
        user: user,
        getInforUser: getInforUser,
    };

    useEffect(() => {
        getInforUser();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <AppContext.Provider value={{ ...value }}>
            {children}
        </AppContext.Provider>
    );
}
