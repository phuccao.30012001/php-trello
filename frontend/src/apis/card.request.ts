import axios from 'axios';
import { AddNewCardType, CardInforType } from 'src/types/card';
import { ColumnInforType } from 'src/types/column';

export const getlistCardRequest = async (payload: ColumnInforType) => {
    const url = process.env.REACT_APP_BACKEND_URL + '/card/list/' + payload.id;
    return axios.get(url);
};

export const addCardRequest = async(payload:AddNewCardType)=>{
    const url = process.env.REACT_APP_BACKEND_URL+'/card/add';
    return axios.post(url,payload)
}

export const updateCardRequest = async (payload:Omit<CardInforType,'column_id'>)=>{
    const url = process.env.REACT_APP_BACKEND_URL+'/card/update/'+ payload.id;
    return axios.put(url,payload)
}

export const deleteCardRequest = async (payload:CardInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL+'/card/delete/'+ payload.id;
    return axios.delete(url)
}

export const deleteFileRequest = async (id:string)=>{
    const url = process.env.REACT_APP_BACKEND_URL+'/file/delete/'+ id;
    return axios.delete(url)
}
