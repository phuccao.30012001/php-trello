import axios from "axios";
import { CardInforType } from "src/types/card";
import { AddMemberType, MemberInforType } from "src/types/member";

export const getListMemberRequest = async (payload:CardInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + '/member/list/' + payload.id;
    return axios.get(url);
}

export const deleteMemberRequest = async (payload:MemberInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + '/member/delete/' + payload.id;
    return axios.delete(url)
}

export const addMemberRequest = async (payload:AddMemberType[])=>{
    const payloadMembers = {
        members:payload
    }
    const url = process.env.REACT_APP_BACKEND_URL + '/member/add';
    return axios.post(url,payloadMembers)
}

export const getListAvailableMemberRequest = async (payload:CardInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + '/member/available/'+payload.id;
    return axios.get(url)
}