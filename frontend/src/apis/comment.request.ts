import axios from "axios"
import { CardInforType } from "src/types/card";
import { AddCommentType } from "src/types/comment";

export const addCommentRequest = (payload: AddCommentType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/comment/add";
    return axios.post(url,payload);
}

export const getListCommentRequest = (payload: CardInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/comment/list/"+payload.id;
    return axios.get(url)
}