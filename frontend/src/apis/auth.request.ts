import axios from "axios"
import { UserInforType, UserLoginType, UserRegisterType } from "src/types/user";

export const loginRequest = (payload:UserLoginType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/user/login";
    return axios.post(url,payload)
}

export const registerRequest = (payload:UserRegisterType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/user/register"
    return axios.post(url,payload)
}

export const getProfile = ()=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/user/profile";
    return axios.get(url)
}

export const updateProfile = (payload:UserInforType)=>{
    const url =process.env.REACT_APP_BACKEND_URL + "/user/update";
    return axios.put(url,payload)
}
