import axios from "axios";
import { ColumnInforType } from "src/types/column";

export const addColumnRequest = async (payload: ColumnInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/column/add";
    return axios.post(url,payload)
}

export const updateColumnRequest = async (payload: ColumnInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/column/update/"+payload.id;
    return axios.put(url,payload)
}

export const deleteColumnRequest = async (payload: ColumnInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/column/delete/"+payload.id;
    return axios.delete(url)
}

export const getlistColumnRequest = async ()=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/column/list";
    return axios.get(url);
}