import axios from "axios"
import { CardInforType } from "src/types/card";
import { AddChecklistType, ChecklistInforType } from "src/types/checklist";

export const getChecklistRequest = async (payload:CardInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + '/checklist/list/' + payload.id;
    return axios.get(url);
}

export const addChecklistRequest = async (payload:AddChecklistType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + '/checklist/add';
    return axios.post(url,payload);
}

export const updateChecklistRequest = async (payload:ChecklistInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + '/checklist/update/'+payload.id;
    return axios.put(url,payload);
}

export const deleteChecklistRequest = async (payload:ChecklistInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + '/checklist/delete/'+payload.id;
    return axios.delete(url);
}