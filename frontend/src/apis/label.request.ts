import axios from "axios";
import { CardInforType } from "src/types/card";
import { AddNewLabelType, LabelInforType } from "src/types/label";
// import { LabelInforType } from "src/types/label";

export const getListLabelRequest = async(payload:CardInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/label/list/"+payload.id;
    return axios.get(url);
}

export const addLabelRequest = async (payload:AddNewLabelType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/label/add";
    return axios.post(url,payload);
}

export const updateLabelRequest = async (payload: LabelInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/label/update/" + payload.id;
    return axios.put(url,payload);
}

export const deleteLabelRequest = async(payload: LabelInforType)=>{
    const url = process.env.REACT_APP_BACKEND_URL + "/label/delete/" + payload.id;
    return axios.delete(url);
}