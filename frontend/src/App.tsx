import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import LoginPage from './pages/user/login';
import RegisterPage from './pages/user/register';
import ProfilePage from './pages/user/profile';
import AppProvider from 'src/context/AppProvider';
import ManagePage from './pages/manage';

function App() {
    return (
        <BrowserRouter>
            <AppProvider>
                <Routes>
                    <Route element={<LoginPage />} path='/login' />
                    <Route element={<RegisterPage />} path='/register' />
                    <Route element={<ProfilePage />} path='/profile' />
                    <Route element={<ManagePage />} path='/manage' />
                </Routes>
            </AppProvider>
        </BrowserRouter>
    );
}

export default App;
