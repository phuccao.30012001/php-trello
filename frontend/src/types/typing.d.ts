import { AxiosResponse } from "axios";

export type ErrorType = {
    response: AxiosResponse<ResponseType>;
}
export type ResponseType = {
    status: boolean;
    message: string
    data?: any;
};
export type TokenResponseType = {
    accessToken: string;
    refreshToken: string
}

export type LabelSelectType = {
    label: React.ReactNode
    value: string
}

export type UploadResponseType = {
  public_id: string;
  secure_url: string;
};

