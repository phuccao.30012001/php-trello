
export type MemberInforType =AddMemberType& {
    fullname:string
    id:number
}

export type AddMemberType = {
    card_id:number
    user_id:number
}