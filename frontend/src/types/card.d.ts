import type {Dayjs} from "dayjs";

export type AddNewCardType = {
    title: string;
    column_id:number
}

export type FormAddNewCardType = {
    title: string;
}

export type CardInforType =AddNewCardType& {
    id:number;
    start: string;
    end:string;
    description: string;
    status:number
    attach_url?: string
}  

export type FormUpdateCardType = FormAddNewCardType&{
    rangedate:Dayjs[]
    description: string;
    status:number;
}