
export type AddNewLabelType = {
    title:string
    color:string
    card_id:number
}
export type LabelInforType = AddNewLabelType & {
    id:number
}