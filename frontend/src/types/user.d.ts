export type UserInforType = {
    id: number;
    fullname: string;
}

export type UserLoginType = {
    email: string;
    password: string;
}

export type UserRegisterType = {
    fullname: string;
    repassword: string;
}&UserLoginType;

export type UserResponseType = {
    id: int;
    fullname: string;
}