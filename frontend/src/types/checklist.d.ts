export type ChecklistInforType = {
    id: number
    status: number
}&AddChecklistType

export type AddChecklistType = {
    content: string
    card_id: int
}