export type AddCommentType = {
    card_id: number
    user_id: number
    content: string
}

export type FormAddType = {
    content: string
}

export type CommentInforType = Omit<AddCommentType,'user_id'>&{
    id: number
    fullname: string
}