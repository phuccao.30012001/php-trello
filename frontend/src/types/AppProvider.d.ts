import { UserInforType } from './user';
export interface IAppContext extends UserType {
    user: UserInforType | undefined;
    // eslint-disable-next-line no-unused-vars
    getInforUser: ()=>Promise<void>
}
