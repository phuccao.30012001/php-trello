export type ColumnInforType = {
    id: number;
    title: string;
}