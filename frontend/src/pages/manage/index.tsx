import DrawerAddColumn from 'src/components/drawer/drawer-add-column';
import { Button, Col, Row, Space } from 'antd';
import { useEffect, useState } from 'react';
import { PlusCircleOutlined } from '@ant-design/icons';
import DrawerUpdateColumn from 'src/components/drawer/drawer-update-column';
import { ColumnInforType } from 'src/types/column';
import ModalDeleteColumn from 'src/components/modal/modal-delete-column';
import LayoutCustom from 'src/components/layout/layout';

import style from 'src/styles/layout.module.scss';
import { getlistColumnRequest } from 'src/apis/column.request';
import { ResponseType } from 'src/types/typing';
import ColumnBox from 'src/components/manage/column-box';

function ManagePage() {
    const [openDrawerAddColumn, setOpenDrawerAddColumn] = useState(false);
    const [openDrawerUpdateColumn, setOpenDrawerUpdateColumn] = useState(false);
    const [openModalDeleteColumn, setOpenModalDeleteColumn] = useState(false);
    const [currentColumn, setCurrentColumn] = useState<ColumnInforType>();
    const [listColumn, setListColumn] = useState<ColumnInforType[]>();

    const onCloseDrawerAddColumn = () => {
        getListColumn();
        setOpenDrawerAddColumn(false);
    };

    const onOpenDrawerAddColumn = () => {
        setOpenDrawerAddColumn(true);
    };

    const onCloseDrawerUpdateColumn = () => {
        getListColumn();
        setCurrentColumn(undefined);
        setOpenDrawerUpdateColumn(false);
    };

    const onOpenDrawerUpdateColumn = (column: ColumnInforType) => {
        setCurrentColumn(column);
        setOpenDrawerUpdateColumn(true);
    };

    const onOpenModalDeleteColumn = (column: ColumnInforType) => {
        setCurrentColumn(column);
        setOpenModalDeleteColumn(true);
    };
    const onCloseModalDeleteColumn = () => {
        getListColumn();
        setCurrentColumn(undefined);
        setOpenModalDeleteColumn(false);
    };

    const getListColumn = async () => {
        try {
            const result: ResponseType = (await getlistColumnRequest()).data;
            if (result.status) {
                setListColumn(result.data);
            }
        } catch (error) {}
    };

    useEffect(() => {
        getListColumn();
    }, []);
    return (
        <LayoutCustom>
            <div className={style['content-container']}>
                <Row justify='end' className={style['toolbar-container']}>
                    <Col span={24}>
                        <Button onClick={onOpenDrawerAddColumn} type='primary'>
                            <PlusCircleOutlined />
                            Thêm cột mới
                        </Button>
                    </Col>
                </Row>
                <Space className={style['list-column-container']}>
                    {listColumn?.map((column: ColumnInforType) => {
                        return (
                            <ColumnBox
                                key={column.id}
                                onOpenDrawerUpdateColumn={
                                    onOpenDrawerUpdateColumn
                                }
                                onOpenModalDeleteColumn={
                                    onOpenModalDeleteColumn
                                }
                                columnInfor={column}
                            />
                        );
                    })}
                </Space>
            </div>
            <DrawerAddColumn
                open={openDrawerAddColumn}
                onClose={onCloseDrawerAddColumn}
            />
            {currentColumn && (
                <DrawerUpdateColumn
                    open={openDrawerUpdateColumn}
                    onClose={onCloseDrawerUpdateColumn}
                    column={currentColumn}
                />
            )}
            {currentColumn && (
                <ModalDeleteColumn
                    open={openModalDeleteColumn}
                    column={currentColumn}
                    onClose={onCloseModalDeleteColumn}
                />
            )}
        </LayoutCustom>
    );
}

export default ManagePage;
