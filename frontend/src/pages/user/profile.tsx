import { AppContext } from 'src/context/AppProvider';
import { useContext, useState } from 'react';
import style from 'src/styles/user.module.scss';
import { Button, Col, Row, Typography } from 'antd';
import { EditOutlined, LogoutOutlined } from '@ant-design/icons';
import DrawerUpdateInformation from 'src/components/drawer/drawer-update-user-infor';
import { removeToken } from 'src/utils/storeToken';
import { useNavigate } from 'react-router-dom';
import LayoutCustom from 'src/components/layout/layout';

const { Title, Text } = Typography;

function ProfilePage() {
    const router = useNavigate();
    const { user } = useContext(AppContext);
    const [open, setOpen] = useState(false);
    //function handle close drawer to update information

    const onClose = () => {
        setOpen(false);
    };

    const onOpen = () => {
        setOpen(true);
    };

    const onLogout = () => {
        removeToken();
        router('/login');
    };
    return (
        <LayoutCustom>
            <Row justify={'center'} className={style['profile']}>
                <Col>
                    <Row>
                        <Title>Thông tin người dùng</Title>
                    </Row>
                    <Row className={style['infor-row']}>
                        <Text className={style['title']}>Tên người dùng</Text>
                        <Text className={style['content']}>
                            {user?.fullname}
                        </Text>
                    </Row>
                    <Row justify={'space-between'}>
                        <Button danger onClick={onLogout}>
                            <LogoutOutlined />
                            Đăng xuất
                        </Button>
                        <Button onClick={onOpen} type='primary' ghost>
                            <EditOutlined />
                            Chỉnh sửa trang cá nhân
                        </Button>
                    </Row>
                </Col>
            </Row>
            {user && (
                <DrawerUpdateInformation
                    open={open}
                    onClose={onClose}
                    user={user}
                />
            )}
        </LayoutCustom>
    );
}

export default ProfilePage;
