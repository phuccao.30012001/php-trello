import { Button, Form, FormInstance, Input, Row, Typography } from 'antd';
import { ErrorType, ResponseType } from 'src/types/typing';
import { UserRegisterType } from 'src/types/user';
import openNotification from 'src/utils/openNotification';
import { registerRequest } from 'src/apis/auth.request';
import style from 'src/styles/user.module.scss';
import { useState } from 'react';
import { LockOutlined, MailOutlined, UserOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';

const { Title } = Typography;

function RegisterPage() {
    const [form] = useState<FormInstance<UserRegisterType>>();
    const router = useNavigate();

    const onFinish = async (payload: UserRegisterType) => {
        try {
            if (payload.password !== payload.repassword)
                openNotification('Lỗi', 'Yêu cầu xác nhận lại mật khẩu');
            else {
                const result: ResponseType = (await registerRequest(payload))
                    .data;
                openNotification('Phản hồi', result.message);
                router('/login');
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    return (
        <>
            <Row justify={'center'}>
                <Title>Đăng ký</Title>
            </Row>
            <Row justify={'center'}>
                <Form
                    form={form}
                    name='basic'
                    onFinish={onFinish}
                    autoComplete='off'
                    className={style['form-user']}
                >
                    <Form.Item
                        name='fullname'
                        rules={[
                            {
                                required: true,
                                message: 'Không được để trống',
                            },
                            {
                                message:
                                    'Tên chỉ bao gồm chữ cái và khoảng trắng',
                                pattern: RegExp('[A-Za-z]+\\s?$'),
                            },
                        ]}
                    >
                        <Input placeholder='Tên' prefix={<UserOutlined />} />
                    </Form.Item>

                    <Form.Item
                        name='email'
                        rules={[
                            {
                                required: true,
                                message: 'Không để trống',
                            },
                            {
                                message: 'Không đúng định dạng email',
                                pattern: RegExp(
                                    '[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'
                                ),
                            },
                        ]}
                    >
                        <Input placeholder='Email' prefix={<MailOutlined />} />
                    </Form.Item>

                    <Form.Item
                        name='password'
                        rules={[
                            {
                                required: true,
                                message: 'Không để trống',
                            },
                            {
                                pattern: RegExp(
                                    '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})'
                                ),
                                message:
                                    '8 kí tự trở lên. Ít nhất 1 chữ hoa, 1 chữ số, 1 kí tự đặc biệt',
                            },
                        ]}
                    >
                        <Input.Password
                            placeholder='Mật khẩu'
                            prefix={<LockOutlined />}
                        />
                    </Form.Item>
                    <Form.Item
                        name='repassword'
                        rules={[
                            {
                                required: true,
                                message: 'Không để trống',
                            },
                        ]}
                    >
                        <Input.Password
                            placeholder='Nhập lại mật khẩu'
                            prefix={<LockOutlined />}
                        />
                    </Form.Item>

                    <Form.Item wrapperCol={{ span: 24 }}>
                        <Button
                            type='primary'
                            htmlType='submit'
                            className={style['btn-submit']}
                        >
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Row>
        </>
    );
}

export default RegisterPage;
