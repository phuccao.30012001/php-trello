import { Button, Form, Input, Row, Typography } from 'antd';
import { Link, useNavigate } from 'react-router-dom';
import { ErrorType, ResponseType } from 'src/types/typing';
import { UserLoginType } from 'src/types/user';
import openNotification from 'src/utils/openNotification';
import { loginRequest } from 'src/apis/auth.request';
import style from 'src/styles/user.module.scss';
import { LockOutlined, MailOutlined } from '@ant-design/icons';
import { storeToken } from 'src/utils/storeToken';
import { useContext } from 'react';
import { AppContext } from 'src/context/AppProvider';

const { Title } = Typography;

function LoginPage() {
    const router = useNavigate();
    const { getInforUser } = useContext(AppContext);
    const onFinish = async (payload: UserLoginType) => {
        try {
            const result: ResponseType = (await loginRequest(payload)).data;
            storeToken({
                accessToken: result.data.accessToken,
                refreshToken: result.data.refreshToken,
            });
            openNotification('Thành công', result.message);
            getInforUser();
            router('/manage');
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    return (
        <>
            <Row justify={'center'}>
                <Title>Đăng nhập</Title>
            </Row>
            <Row justify={'center'}>
                <Form
                    name='basic'
                    onFinish={onFinish}
                    autoComplete='off'
                    className={style['form-user']}
                >
                    <Form.Item
                        wrapperCol={{ offset: 0, span: 24 }}
                        name='email'
                        rules={[
                            {
                                required: true,
                                message: 'Không để trống',
                            },
                            {
                                message: 'Không đúng định dạng email',
                                pattern: RegExp(
                                    '[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'
                                ),
                            },
                        ]}
                    >
                        <Input placeholder='Email' prefix={<MailOutlined />} />
                    </Form.Item>

                    <Form.Item
                        wrapperCol={{ offset: 0, span: 24 }}
                        name='password'
                        rules={[
                            {
                                required: true,
                                message: 'Không để trống',
                            },
                        ]}
                    >
                        <Input.Password
                            placeholder='Mật khẩu'
                            prefix={<LockOutlined />}
                        />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 0, span: 24 }}>
                        <Button
                            type='primary'
                            htmlType='submit'
                            className={style['btn-submit']}
                        >
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Row>
            <Row justify={'center'}>
                <Link to={'/register'}>Bạn chưa có tài khoản?</Link>
            </Row>
        </>
    );
}

export default LoginPage;
