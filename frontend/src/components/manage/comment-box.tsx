import { Row, Typography } from 'antd';
import React from 'react';
import { CommentInforType } from 'src/types/comment';
import style from 'src/styles/card.module.scss';

type CommentBoxProps = {
    listComment: CommentInforType[];
};

function CommentBox({ listComment }: CommentBoxProps) {
    return (
        // <div className={style['cmt-box']}>
        <div className={style['cmt-box-container']} id='cmt-box'>
            {listComment?.map((comment) => {
                return (
                    <Row key={comment.id} className={style['cmt-gr']}>
                        <p className={style['cmt-title']}>{comment.fullname}</p>
                        <p className={style['cmt-content']}>
                            {comment.content}
                        </p>
                    </Row>
                );
            })}
        </div>
        // </div>
    );
}

export default CommentBox;
