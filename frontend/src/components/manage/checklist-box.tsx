import { Button, Checkbox, Dropdown, MenuProps, Row, Typography } from 'antd';
import ModalAddChecklist from '../modal/modal-add-checklist';
import { useEffect, useState } from 'react';
import { ErrorType, ResponseType } from 'src/types/typing';
import {
    getChecklistRequest,
    updateChecklistRequest,
} from 'src/apis/checklist.request';
import { CardInforType } from 'src/types/card';
import { ChecklistInforType } from 'src/types/checklist';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import style from 'src/styles/card.module.scss';
import ModalUpdateChecklist from '../modal/modal-update-checklist';
import ModalDeleteChecklist from '../modal/modal-delete-checklist';
import openNotification from 'src/utils/openNotification';

type ChecklistBoxProps = { card: CardInforType };

function ChecklistBox({ card }: ChecklistBoxProps) {
    const [checklist, setChecklist] = useState<ChecklistInforType[]>();

    const [openModalAddChecklist, setOpenModalAddChecklist] = useState(false);
    const [currentChecklist, setCurrentChecklist] =
        useState<ChecklistInforType>();
    const [openModalUpdateChecklist, setOpenModalUpdateChecklist] =
        useState(false);
    const [openModalDeleteChecklist, setOpenModalDeleteChecklist] =
        useState(false);

    const onOpenModalDeleteChecklist = () => {
        setOpenModalDeleteChecklist(true);
    };

    const onCloseModalDeleteChecklist = () => {
        getChecklist();
        setOpenModalDeleteChecklist(false);
        setCurrentChecklist(undefined);
    };

    const onOpenModalUpdateChecklist = () => {
        setOpenModalUpdateChecklist(true);
    };

    const onCloseModalUpdateChecklist = () => {
        getChecklist();
        setOpenModalUpdateChecklist(false);
        setCurrentChecklist(undefined);
    };

    const onOpenModalAddChecklist = () => {
        setOpenModalAddChecklist(true);
    };

    const onCloseModalAddChecklist = () => {
        getChecklist();
        setOpenModalAddChecklist(false);
    };

    const onFinishTask = async (
        checklist: ChecklistInforType,
        checked: boolean
    ) => {
        try {
            const payload: ChecklistInforType = {
                id: Number(checklist.id),
                card_id: Number(checklist.card_id),
                content: checklist.content,
                status: checked ? 1 : 0,
            };

            await updateChecklistRequest(payload);
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    const getChecklist = async () => {
        try {
            const result: ResponseType = (await getChecklistRequest(card)).data;
            if (result.status) {
                setChecklist(result.data);
            }
        } catch (error) {}
    };

    const handleButtonClick = (
        e: React.MouseEvent<HTMLElement, MouseEvent>
    ) => {
        e.preventDefault();
    };

    const items: MenuProps['items'] = [
        {
            label: <Typography.Text>Chỉnh sửa</Typography.Text>,
            key: '1',
            icon: <EditOutlined />,
        },
        {
            label: <Typography.Text>Xóa</Typography.Text>,
            key: '2',
            icon: <DeleteOutlined />,
        },
    ];

    useEffect(() => {
        getChecklist();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <>
            <Row>
                {checklist &&
                    checklist?.map((checklist) => {
                        return (
                            <div
                                key={checklist.id}
                                className={style['checklist-box']}
                            >
                                <Checkbox
                                    defaultChecked={!!Number(checklist.status)}
                                    className={style['checkbox']}
                                    onChange={(value) => {
                                        onFinishTask(
                                            checklist,
                                            value.target.checked
                                        );
                                    }}
                                />
                                <Dropdown.Button
                                    menu={{
                                        items: items,
                                        onClick: ({ key }) => {
                                            if (Number(key) === 1) {
                                                onOpenModalUpdateChecklist();
                                            } else {
                                                onOpenModalDeleteChecklist();
                                            }
                                            setCurrentChecklist(checklist);
                                        },
                                    }}
                                    onClick={handleButtonClick}
                                    trigger={['click']}
                                >
                                    {checklist.content}
                                </Dropdown.Button>
                            </div>
                        );
                    })}
                <Button onClick={onOpenModalAddChecklist} type='primary' ghost>
                    Thêm
                </Button>
            </Row>

            {
                <ModalAddChecklist
                    open={openModalAddChecklist}
                    onClose={onCloseModalAddChecklist}
                    card={card}
                />
            }
            {currentChecklist && (
                <ModalUpdateChecklist
                    open={openModalUpdateChecklist}
                    onClose={onCloseModalUpdateChecklist}
                    checklist={currentChecklist}
                />
            )}
            {currentChecklist && (
                <ModalDeleteChecklist
                    open={openModalDeleteChecklist}
                    onClose={onCloseModalDeleteChecklist}
                    checklist={currentChecklist}
                />
            )}
        </>
    );
}

export default ChecklistBox;
