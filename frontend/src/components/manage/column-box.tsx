import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Card, Row, Space, Typography } from 'antd';
import { CardInforType } from 'src/types/card';
import style from 'src/styles/column.module.scss';
import { useEffect, useState } from 'react';
import { ResponseType } from 'src/types/typing';
import { getlistCardRequest } from 'src/apis/card.request';
import { ColumnInforType } from 'src/types/column';
import ModalAddCard from 'src/components/modal/modal-add-card';
import CardBox from 'src/components/manage/card-box';
import DrawerEditCard from '../drawer/drawer-edit-card';

type CardBoxProps = {
    columnInfor: ColumnInforType;
    onOpenDrawerUpdateColumn: (column: ColumnInforType) => void;
    onOpenModalDeleteColumn: (column: ColumnInforType) => void;
};
function ColumnBox({
    columnInfor,
    onOpenDrawerUpdateColumn,
    onOpenModalDeleteColumn,
}: CardBoxProps) {
    const [openModalAddCard, setOpenModalAddCard] = useState(false);
    const [openDrawerUpdateCard, setOpenDrawerUpdateCard] = useState(false);
    const [currentCard, setCurrentCard] = useState<CardInforType>();
    const [listCard, setListCard] = useState<CardInforType[]>();

    const getListCard = async () => {
        try {
            const result: ResponseType = (await getlistCardRequest(columnInfor))
                .data;
            if (result.status) {
                setListCard(result.data);
            }
        } catch (error) {}
    };

    const onCloseModalAddCard = () => {
        getListCard();
        setOpenModalAddCard(false);
    };

    const onOpenModalAddCard = () => {
        setOpenModalAddCard(true);
    };

    const onCloseDrawerUpdateCard = () => {
        getListCard();
        setCurrentCard(undefined);
        setOpenDrawerUpdateCard(false);
    };

    const onOpenDrawerUpdateCard = (card: CardInforType) => {
        setCurrentCard(card);
        setOpenDrawerUpdateCard(true);
    };

    useEffect(() => {
        getListCard();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const TitleCard = () => {
        return (
            <Row justify='space-between'>
                <Typography.Text
                    className={style['title-card-box']}
                    ellipsis={{ tooltip: columnInfor.title }}
                >
                    {columnInfor.title}
                </Typography.Text>
                <Space>
                    <Button
                        type='text'
                        onClick={() => onOpenDrawerUpdateColumn(columnInfor)}
                        className={style['btn-card']}
                    >
                        <EditOutlined />
                    </Button>
                    <Button
                        type='text'
                        onClick={() => onOpenModalDeleteColumn(columnInfor)}
                        className={style['btn-card']}
                    >
                        <DeleteOutlined />
                    </Button>
                    <Button
                        type='text'
                        className={style['btn-card']}
                        onClick={onOpenModalAddCard}
                    >
                        <PlusOutlined />
                    </Button>
                </Space>
            </Row>
        );
    };
    return (
        <>
            <Card className={style['card-container']} title={<TitleCard />}>
                {listCard &&
                    listCard.map((card: CardInforType) => {
                        return (
                            <div
                                key={card.id}
                                className={style['card-item']}
                                onClick={() => {
                                    onOpenDrawerUpdateCard(card);
                                }}
                            >
                                <CardBox card={card} />
                            </div>
                        );
                    })}
            </Card>
            <ModalAddCard
                open={openModalAddCard}
                onClose={onCloseModalAddCard}
                columnId={columnInfor.id}
            />
            {currentCard && (
                <DrawerEditCard
                    open={openDrawerUpdateCard}
                    onClose={onCloseDrawerUpdateCard}
                    card={currentCard}
                />
            )}
        </>
    );
}

export default ColumnBox;
