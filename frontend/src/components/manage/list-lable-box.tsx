import { Button, Col, Row } from 'antd';
import { useEffect, useState } from 'react';
import { CardInforType } from 'src/types/card';
import { LabelInforType } from 'src/types/label';
import { PlusOutlined } from '@ant-design/icons';
import ModalAddLabel from '../modal/modal-add-label';
import ModalUpdateLabel from '../modal/modal-update-label';
import { getListLabelRequest } from 'src/apis/label.request';
import { ResponseType } from 'src/types/typing';

type ListLabelProps = {
    listLabel: LabelInforType[];
    onOpenModalUpdateLabel: (label: LabelInforType) => void;
    onOpenModalAddLabel: () => void;
};
function ListLabel({
    listLabel,
    onOpenModalUpdateLabel,
    onOpenModalAddLabel,
}: ListLabelProps) {
    return (
        <Row gutter={5}>
            {listLabel.map((label: LabelInforType) => {
                return (
                    <Col key={label.id}>
                        <Button
                            style={{
                                backgroundColor: label.color,
                            }}
                            type='primary'
                            onClick={() => {
                                onOpenModalUpdateLabel(label);
                            }}
                        >
                            {label.title}
                        </Button>
                    </Col>
                );
            })}
            <Col>
                <Button type='primary' ghost onClick={onOpenModalAddLabel}>
                    <PlusOutlined />
                </Button>
            </Col>
        </Row>
    );
}

type ListLableBoxProps = {
    card: CardInforType;
};
function ListLableBox({ card }: ListLableBoxProps) {
    const [listLabel, setListLabel] = useState<LabelInforType[]>();

    const [currentLabel, setCurrentLabel] = useState<LabelInforType>();
    const [openModalAddLabel, setOpenModalAddLabel] = useState<boolean>(false);

    const [openModalUpdateLabel, setOpenModalUpdateLabel] =
        useState<boolean>(false);

    const onOpenModalUpdateLabel = (label: LabelInforType) => {
        setCurrentLabel(label);
        setOpenModalUpdateLabel(true);
    };

    const onCloseModalUpdateLabel = () => {
        getListLabel();
        setCurrentLabel(undefined);
        setOpenModalUpdateLabel(false);
    };

    const onOpenModalAddLabel = () => {
        setOpenModalAddLabel(true);
    };

    const onCloseModalAddLabel = () => {
        getListLabel();
        setOpenModalAddLabel(false);
    };
    const getListLabel = async () => {
        try {
            const result: ResponseType = (await getListLabelRequest(card)).data;
            if (result.status) {
                setListLabel(result.data);
            }
        } catch (error) {
            setListLabel([]);
        }
    };
    useEffect(() => {
        getListLabel();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <>
            {listLabel && (
                <ListLabel
                    listLabel={listLabel}
                    onOpenModalAddLabel={onOpenModalAddLabel}
                    onOpenModalUpdateLabel={onOpenModalUpdateLabel}
                />
            )}
            {openModalAddLabel && (
                <ModalAddLabel
                    open={openModalAddLabel}
                    card={card}
                    onClose={onCloseModalAddLabel}
                />
            )}
            {currentLabel && openModalUpdateLabel && (
                <ModalUpdateLabel
                    open={openModalUpdateLabel}
                    label={currentLabel}
                    onClose={onCloseModalUpdateLabel}
                />
            )}
        </>
    );
}

export default ListLableBox;
