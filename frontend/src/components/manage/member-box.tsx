import { Button, Col, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import {
    deleteMemberRequest,
    getListMemberRequest,
} from 'src/apis/member.request';
import { CardInforType } from 'src/types/card';
import { ErrorType, ResponseType } from 'src/types/typing';
import style from 'src/styles/card.module.scss';
import { CloseOutlined, PlusOutlined } from '@ant-design/icons';
import { MemberInforType } from 'src/types/member';
import openNotification from 'src/utils/openNotification';
import ModalAddMember from '../modal/modal-add-member';

type ListMemberProps = {
    listMember: MemberInforType[];
    deleteMember: (payload: MemberInforType) => Promise<void>;
    onOpenModalAddMember: () => void;
};

function ListMember({
    listMember,
    deleteMember,
    onOpenModalAddMember,
}: ListMemberProps) {
    return (
        <Row gutter={5}>
            {listMember.map((member: MemberInforType) => {
                return (
                    <Col key={member.id}>
                        <div className={style['member-card-box']}>
                            {member.fullname}
                            <Button
                                className={style['btn-delete-member']}
                                type='text'
                                onClick={() => {
                                    deleteMember(member);
                                }}
                            >
                                <CloseOutlined />
                            </Button>
                        </div>
                    </Col>
                );
            })}
            <Col>
                <Button onClick={onOpenModalAddMember} type='primary' ghost>
                    <PlusOutlined />
                </Button>
            </Col>
        </Row>
    );
}

type MemberBoxProps = {
    card: CardInforType;
};

function MemberBox({ card }: MemberBoxProps) {
    const [listMember, setListMember] = useState<MemberInforType[]>();
    const [openModalAddMember, setOpenModalAddMember] = useState(false);

    const onOpenModalAddMember = () => {
        setOpenModalAddMember(true);
    };

    const onCloseModalAddMember = () => {
        getListMember();
        setOpenModalAddMember(false);
    };

    const getListMember = async () => {
        try {
            const result: ResponseType = (await getListMemberRequest(card))
                .data;
            if (result.status) {
                setListMember(result.data);
            }
        } catch (error) {
            setListMember([]);
        }
    };
    const deleteMember = async (payload: MemberInforType) => {
        try {
            const result: ResponseType = (await deleteMemberRequest(payload))
                .data;
            if (result.status) {
                openNotification('Thành công', result.message);
                await getListMember();
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };
    useEffect(() => {
        getListMember();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            {listMember && (
                <ListMember
                    listMember={listMember}
                    deleteMember={deleteMember}
                    onOpenModalAddMember={onOpenModalAddMember}
                />
            )}
            {openModalAddMember && (
                <ModalAddMember
                    card={card}
                    open={openModalAddMember}
                    onClose={onCloseModalAddMember}
                />
            )}
        </>
    );
}

export default MemberBox;
