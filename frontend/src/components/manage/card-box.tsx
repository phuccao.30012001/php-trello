import { Card, Row, Typography } from 'antd';
import { useEffect, useState } from 'react';
import { getListLabelRequest } from 'src/apis/label.request';
import { CardInforType } from 'src/types/card';
import { LabelInforType } from 'src/types/label';
import { ResponseType } from 'src/types/typing';
import style from 'src/styles/card.module.scss';

type CardBoxProps = {
    card: CardInforType;
};

type ListLabelProps = {
    listLabel: LabelInforType[];
};
const ListLabel = ({ listLabel }: ListLabelProps) => {
    return (
        <Row>
            {listLabel &&
                listLabel.map((label: LabelInforType) => {
                    return (
                        <div
                            key={label.id}
                            style={{ backgroundColor: label.color }}
                            className={style['label-item']}
                        />
                    );
                })}
        </Row>
    );
};

function CardBox({ card }: CardBoxProps) {
    const [listLabel, setListLabel] = useState<LabelInforType[]>();
    const getListLabel = async () => {
        try {
            const result: ResponseType = (await getListLabelRequest(card)).data;
            if (result.status) {
                setListLabel(result.data);
            }
        } catch (error) {}
    };
    useEffect(() => {
        getListLabel();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [card]);
    return (
        <>
            <Card>
                <ListLabel listLabel={listLabel || []} />
                <Typography.Text>{card.title}</Typography.Text>
            </Card>
        </>
    );
}

export default CardBox;
