import {
    Button,
    Checkbox,
    Col,
    DatePicker,
    Drawer,
    Form,
    Input,
    Row,
    Space,
    Typography,
    Upload,
    UploadFile,
} from 'antd';
import { useState } from 'react';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
import { CardInforType, FormUpdateCardType } from 'src/types/card';
import dayjs from 'dayjs';
import { updateCardRequest } from 'src/apis/card.request';
import ModalDeleteCard from '../modal/modal-delete-card';
import ListLableBox from '../manage/list-lable-box';
import MemberBox from '../manage/member-box';
import ChecklistBox from '../manage/checklist-box';
import style from 'src/styles/card.module.scss';
import { LinkOutlined, UploadOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { UploadChangeParam } from 'antd/es/upload';
import ModalComment from '../modal/modal-comment';

type DrawerEditCardProps = {
    open: boolean;
    onClose: () => void;
    card: CardInforType;
};

function DrawerEditCard({ open, onClose, card }: DrawerEditCardProps) {
    const actionUrl = process.env.REACT_APP_BACKEND_URL + '/file/upload';
    const [form] = Form.useForm();
    const [openModalDeleteCard, setOpenModalDeleteCard] = useState(false);
    const [openModalComment, setOpenModalComment] = useState(false);

    const [url, setUrl] = useState<string>(card.attach_url || ' ');

    const onOpenModalComment = () => {
        setOpenModalComment(true);
    };

    const onCloseModalComment = () => {
        setOpenModalComment(false);
    };

    const onOpenModalDeleteCard = () => {
        setOpenModalDeleteCard(true);
    };

    const onCloseModalDeleteCard = () => {
        setOpenModalDeleteCard(false);
    };

    const onOk = () => {
        onClose();
        setOpenModalDeleteCard(false);
    };

    const onSubmit = async (value: FormUpdateCardType) => {
        try {
            const payload: Omit<CardInforType, 'column_id'> = {
                id: Number(card.id),
                title: value.title,
                description: value.description,
                start: value.rangedate[0].format('YYYY-MM-DD'),
                end: value.rangedate[1].format('YYYY-MM-DD'),
                status: value.status ? 1 : 0,
                attach_url: url || '',
            };
            const result: ResponseType = (await updateCardRequest(payload))
                .data;
            if (result.status) {
                openNotification('Thành công', result.message);
                form.resetFields();
                onClose();
            } else {
                openNotification('Phản hồi', result.message);
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    const onUpload = async ({ file }: UploadChangeParam<UploadFile>) => {
        try {
            console.log(file);
            if (file.status === 'done') {
                setUrl(file.response.data.secure_url);
            } else if (file.status === 'removed') {
                setUrl(' ');
            }
        } catch (error) {}
    };

    return (
        <>
            <Drawer
                title={
                    <div>
                        Thông tin thẻ
                        <Button
                            className={style['btn-cmt']}
                            onClick={onOpenModalComment}
                        >
                            Bình luận
                        </Button>
                    </div>
                }
                width={720}
                onClose={onClose}
                open={open}
                bodyStyle={{ paddingBottom: 80 }}
            >
                <Form layout='vertical' onFinish={onSubmit} form={form}>
                    <Row>
                        <Typography.Title
                            level={5}
                            className={style['title-edit-card']}
                        >
                            Nhãn
                        </Typography.Title>
                    </Row>
                    <ListLableBox card={card} />
                    <Row>
                        <Typography.Title
                            level={5}
                            className={style['title-edit-card']}
                        >
                            Thành viên
                        </Typography.Title>
                    </Row>
                    <MemberBox card={card} />
                    <Row>
                        <Typography.Title
                            level={5}
                            className={style['title-edit-card']}
                        >
                            Danh sách công việc
                        </Typography.Title>
                    </Row>
                    <ChecklistBox card={card} />
                    <Row>
                        <Typography.Title
                            level={5}
                            className={style['title-edit-card']}
                        >
                            Thông tin chung
                        </Typography.Title>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item
                                name='title'
                                label='Tiêu đề'
                                rules={[
                                    {
                                        required: true,
                                        message: 'Không được để trống',
                                    },
                                    {
                                        message:
                                            'Tiêu đề chỉ bao gồm chữ cái, chữ số và khoảng trắng',
                                        pattern: RegExp('[A-Za-z0-9]+\\s?$'),
                                    },
                                ]}
                                initialValue={card.title}
                                className={style['form-item']}
                            >
                                <Input
                                    placeholder='Nhập tiêu đề card'
                                    autoFocus={true}
                                />
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                name='rangedate'
                                label='Có hiệu lực'
                                rules={[
                                    {
                                        required: true,
                                        message: 'Không được để trống',
                                    },
                                ]}
                                initialValue={[
                                    dayjs(card.start),
                                    dayjs(card.end),
                                ]}
                                className={style['form-item']}
                            >
                                <DatePicker.RangePicker />
                            </Form.Item>
                        </Col>
                        <Col span={24}>
                            <Form.Item
                                name='status'
                                initialValue={Number(card.status)}
                                valuePropName='checked'
                                className={style['form-item']}
                            >
                                <Checkbox>Hoàn thành</Checkbox>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name='description'
                                label='Mô tả'
                                initialValue={card.description}
                                className={style['form-item']}
                            >
                                <Input.TextArea rows={4} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item>
                                <Upload
                                    maxCount={1}
                                    onChange={onUpload}
                                    action={actionUrl}
                                    showUploadList={true}
                                    listType='picture'
                                    accept='image/*'
                                >
                                    <Button icon={<UploadOutlined />}>
                                        Select File
                                    </Button>
                                </Upload>
                            </Form.Item>
                            {card.attach_url !== ' ' &&
                                url === card.attach_url && (
                                    <Form.Item>
                                        <Link
                                            to={card.attach_url}
                                            target='_blank'
                                        >
                                            <Button>
                                                Attach
                                                <LinkOutlined />
                                            </Button>
                                        </Link>
                                    </Form.Item>
                                )}
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item>
                                <Space>
                                    <Button onClick={onClose}>Hủy</Button>
                                    <Button
                                        danger
                                        onClick={onOpenModalDeleteCard}
                                    >
                                        Xóa
                                    </Button>
                                    <Button type='primary' htmlType='submit'>
                                        Lưu
                                    </Button>
                                </Space>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Drawer>
            <ModalDeleteCard
                open={openModalDeleteCard}
                card={card}
                onClose={onCloseModalDeleteCard}
                onOk={onOk}
            />
            {openModalComment && (
                <ModalComment
                    open={openModalComment}
                    onClose={onCloseModalComment}
                    card={card}
                />
            )}
        </>
    );
}

export default DrawerEditCard;
