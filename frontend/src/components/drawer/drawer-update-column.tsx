import { Button, Col, Drawer, Form, Input, Row, Space } from 'antd';
import { ColumnInforType } from 'src/types/column';
import openNotification from 'src/utils/openNotification';
import { updateColumnRequest } from 'src/apis/column.request';
import { ErrorType, ResponseType } from 'src/types/typing';

type DrawerUpdateColumnProps = {
    open: boolean;
    onClose: () => void;
    column: ColumnInforType;
};

function DrawerUpdateColumn({
    open,
    onClose,
    column,
}: DrawerUpdateColumnProps) {
    const onSubmit = async (value: ColumnInforType) => {
        try {
            const result: ResponseType = (
                await updateColumnRequest({ ...column, ...value })
            ).data;
            if (result.status) {
                openNotification('Thành công', result.message);
                form.resetFields();
                onClose();
            } else {
                openNotification('Phản hồi', result.message);
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    const [form] = Form.useForm();
    return (
        <Drawer
            title='Chỉnh sửa column'
            width={720}
            onClose={onClose}
            open={open}
            bodyStyle={{ paddingBottom: 80 }}
        >
            <Form layout='vertical' onFinish={onSubmit} form={form}>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item
                            name='title'
                            label='Tiêu đề của cột'
                            rules={[
                                {
                                    required: true,
                                    message: 'Không được để trống',
                                },
                                {
                                    message:
                                        'Tên chỉ bao gồm chữ cái và khoảng trắng',
                                    pattern: RegExp('[A-Za-z0-9]+\\s?$'),
                                },
                            ]}
                            initialValue={column.title}
                        >
                            <Input placeholder='Nhập tiêu đề của cột' />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item>
                            <Space>
                                <Button onClick={onClose}>Hủy</Button>
                                <Button type='primary' htmlType='submit'>
                                    Chỉnh sửa
                                </Button>
                            </Space>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Drawer>
    );
}

export default DrawerUpdateColumn;
