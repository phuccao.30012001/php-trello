import { Button, Col, Drawer, Form, Input, Row, Space } from 'antd';
import { addColumnRequest } from 'src/apis/column.request';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
import { ColumnInforType } from 'src/types/column';

type DrawerAddColumnProps = {
    open: boolean;
    onClose: () => void;
};

function DrawerAddColumn({ open, onClose }: DrawerAddColumnProps) {
    const onSubmit = async (value: ColumnInforType) => {
        try {
            const result: ResponseType = (await addColumnRequest(value)).data;
            if (result.status) {
                openNotification('Thành công', result.message);
                form.resetFields();
                onClose();
            } else {
                openNotification('Phản hồi', result.message);
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    const [form] = Form.useForm();
    return (
        <Drawer
            title='Tạo cột mới'
            width={720}
            onClose={onClose}
            open={open}
            bodyStyle={{ paddingBottom: 80 }}
        >
            <Form layout='vertical' onFinish={onSubmit} form={form}>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item
                            name='title'
                            label='Tiêu đề của cột'
                            rules={[
                                {
                                    required: true,
                                    message: 'Không được để trống',
                                },
                                {
                                    message:
                                        'Tiêu đề chỉ bao gồm chữ cái, chữ số và khoảng trắng',
                                    pattern: RegExp('[A-Za-z0-9]+\\s?$'),
                                },
                            ]}
                        >
                            <Input
                                placeholder='Nhập tiêu đề của cột'
                                autoFocus={true}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item>
                            <Space>
                                <Button onClick={onClose}>Hủy</Button>
                                <Button type='primary' htmlType='submit'>
                                    Tạo mới
                                </Button>
                            </Space>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Drawer>
    );
}

export default DrawerAddColumn;
