import { Button, Col, Drawer, Form, Input, Row, Space } from 'antd';
import { useContext } from 'react';
import { UserInforType } from 'src/types/user';
import { updateProfile } from 'src/apis/auth.request';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
import { AppContext } from 'src/context/AppProvider';

type DrawerUpdateInformationProps = {
    open: boolean;
    onClose: () => void;
    user: UserInforType;
};

function DrawerUpdateInformation({
    open,
    onClose,
    user,
}: DrawerUpdateInformationProps) {
    const [form] = Form.useForm();
    const { getInforUser } = useContext(AppContext);

    const onSubmit = async (value: UserInforType) => {
        try {
            const payload = { ...user, ...value };
            const result: ResponseType = (await updateProfile(payload)).data;
            if (result.status) {
                openNotification('Thành công', result.message);
                form.resetFields();
                getInforUser();
                onClose();
            } else {
                openNotification('Phản hồi', result.message);
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };
    return (
        <Drawer
            title='Chỉnh sửa thông tin người dùng'
            width={720}
            onClose={onClose}
            open={open}
            bodyStyle={{ paddingBottom: 80 }}
        >
            <Form layout='vertical' onFinish={onSubmit} form={form}>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item
                            name='fullname'
                            label='Tên người dùng'
                            rules={[
                                {
                                    required: true,
                                    message: 'Không được để trống',
                                },
                                {
                                    message:
                                        'Tên chỉ bao gồm chữ cái và khoảng trắng',
                                    pattern: RegExp('[A-Za-z]+\\s?$'),
                                },
                            ]}
                            initialValue={user.fullname}
                        >
                            <Input placeholder='Nhập tên người dùng' />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item>
                            <Space>
                                <Button onClick={onClose}>Hủy</Button>
                                <Button type='primary' htmlType='submit'>
                                    Chỉnh sửa
                                </Button>
                            </Space>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Drawer>
    );
}

export default DrawerUpdateInformation;
