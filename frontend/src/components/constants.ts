import { LabelSelectType } from 'src/types/typing';

export const listColor: LabelSelectType[] = [
    {
        label: '#f5222d',
        value: 'Red',
    },
    {
        label: '#fa8c16',
        value: 'Orange',
    },
    {
        label: '#fadb14',
        value: 'Yellow',
    },
    {
        label: '#52c41a',
        value: 'Green',
    },
    {
        label:'#1677ff',
        value:'Blue'
    },
    {
        label:'#eb2f96',
        value:'Pink'
    }
];

export const listRecommendColor = [
    '#f5222d',
    '#fa8c16',
    '#fadb14',
    '#52c41a',
    '#1677ff',
    '#eb2f96',
];