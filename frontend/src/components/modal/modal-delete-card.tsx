import { Modal, Typography } from 'antd';
import { deleteCardRequest } from 'src/apis/card.request';
import { CardInforType } from 'src/types/card';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
type ModalDeleteCardProps = {
    open: boolean;
    onClose: () => void;
    card: CardInforType;
    onOk: () => void;
};
const { Text } = Typography;
function ModalDeleteCard({ open, onClose, card, onOk }: ModalDeleteCardProps) {
    const onSubmit = async () => {
        try {
            const result: ResponseType = (await deleteCardRequest(card)).data;
            if (result.status) {
                openNotification('Thành công', result.message);
                onOk();
            } else {
                openNotification('Phản hồi', result.message);
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };
    return (
        <Modal
            title='Xóa thẻ'
            open={open}
            onOk={onSubmit}
            onCancel={onClose}
            okText='Đồng ý'
            cancelText='Hủy bỏ'
            okButtonProps={{
                danger: true,
            }}
        >
            <Text>Bạn có chắc chắn muốn xóa thẻ này không?</Text>
        </Modal>
    );
}

export default ModalDeleteCard;
