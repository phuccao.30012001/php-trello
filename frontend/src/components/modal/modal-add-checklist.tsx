import { Form, Input, Modal } from 'antd';
import React from 'react';
import { addChecklistRequest } from 'src/apis/checklist.request';
import { CardInforType } from 'src/types/card';
import { AddChecklistType } from 'src/types/checklist';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';

type ModalAddChecklistProps = {
    open: boolean;
    onClose: () => void;
    card: CardInforType;
};

function ModalAddChecklist({ open, onClose, card }: ModalAddChecklistProps) {
    const [form] = Form.useForm();
    const onSubmit = async () => {
        try {
            const payload: AddChecklistType = {
                content: form.getFieldValue('content'),
                card_id: Number(card.id),
            };

            const result: ResponseType = (await addChecklistRequest(payload))
                .data;
            if (result.status) {
                form.resetFields();
                openNotification('Thành công', result.message);
                onClose();
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };
    return (
        <Modal
            title='Thêm công việc'
            open={open}
            onOk={onSubmit}
            onCancel={onClose}
            okText='Đồng ý'
            cancelText='Hủy bỏ'
        >
            <Form layout='vertical' form={form} onFinish={onSubmit}>
                <Form.Item name='content'>
                    <Input placeholder='Tên công việc' />
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalAddChecklist;
