import { Form, Input, Modal } from 'antd';
import { addCardRequest } from 'src/apis/card.request';
import { AddNewCardType } from 'src/types/card';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
type ModalAddCardProps = {
    open: boolean;
    onClose: () => void;
    columnId: number;
};
function ModalAddCard({ open, onClose, columnId }: ModalAddCardProps) {
    const [form] = Form.useForm();
    const onSubmit = async () => {
        try {
            const payload: AddNewCardType = {
                title: form.getFieldValue('title'),
                column_id: Number(columnId),
            };
            const result: ResponseType = (await addCardRequest(payload)).data;
            if (result.status) {
                openNotification('Thành công', result.message);
                form.resetFields();
                onClose();
            } else {
                openNotification('Phản hồi', result.message);
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };
    return (
        <Modal
            title='Thêm thẻ mới'
            open={open}
            onOk={onSubmit}
            onCancel={onClose}
            okText='Đồng ý'
            cancelText='Hủy bỏ'
        >
            <Form layout='vertical' onFinish={onSubmit} form={form}>
                <Form.Item
                    name='title'
                    label='Tiêu đề'
                    rules={[
                        {
                            required: true,
                            message: 'Không được để trống',
                        },
                    ]}
                >
                    <Input
                        placeholder='Nhập tiêu đề của thẻ'
                        autoFocus={true}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalAddCard;
