import { Modal, Typography } from 'antd';
import { deleteChecklistRequest } from 'src/apis/checklist.request';
import { ChecklistInforType } from 'src/types/checklist';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
type ModalDeleteChecklistProps = {
    open: boolean;
    onClose: () => void;
    checklist: ChecklistInforType;
};
const { Text } = Typography;
function ModalDeleteChecklist({
    open,
    onClose,
    checklist,
}: ModalDeleteChecklistProps) {
    const onSubmit = async () => {
        try {
            const result: ResponseType = (
                await deleteChecklistRequest(checklist)
            ).data;
            if (result.status) {
                openNotification('Thành công', result.message);
                onClose();
            } else {
                openNotification('Phản hồi', result.message);
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };
    return (
        <Modal
            title='Xóa công việc'
            open={open}
            onOk={onSubmit}
            onCancel={onClose}
            okText='Đồng ý'
            cancelText='Hủy bỏ'
            okButtonProps={{ danger: true }}
        >
            <Text>Bạn có chắc chắn muốn xóa công việc này không?</Text>
        </Modal>
    );
}

export default ModalDeleteChecklist;
