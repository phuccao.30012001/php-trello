import { Form, Input, Modal, ColorPicker, Button } from 'antd';
import { Color, ColorPickerProps } from 'antd/es/color-picker';
import { useState } from 'react';
import { deleteLabelRequest, updateLabelRequest } from 'src/apis/label.request';
import { LabelInforType } from 'src/types/label';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
import { listRecommendColor } from '../constants';
type ModalUpdateLabelProps = {
    open: boolean;
    onClose: () => void;
    label: LabelInforType;
};
function ModalUpdateLabel({ open, onClose, label }: ModalUpdateLabelProps) {
    const [form] = Form.useForm();

    const [colorRgb, setColorRgb] = useState<Color | string>(label.color);
    const [formatRgb, setFormatRgb] =
        useState<ColorPickerProps['format']>('rgb');
    const onSubmit = async () => {
        const rgbString =
            typeof colorRgb === 'string' ? colorRgb : colorRgb.toRgbString();
        const payload: LabelInforType = {
            color: rgbString,
            id: Number(label.id),
            title: form.getFieldValue('title'),
            card_id: Number(label.card_id),
        };
        const result: ResponseType = (await updateLabelRequest(payload)).data;
        if (result.status) {
            form.resetFields();
            openNotification('Thành công', result.message);
            onClose();
        }
        try {
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    const onDelete = async () => {
        try {
            const result: ResponseType = (await deleteLabelRequest(label)).data;
            if (result.status) {
                form.resetFields();
                openNotification('Thành công', result.message);
                onClose();
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };
    return (
        <Modal
            title='Thông tin label'
            open={open}
            onCancel={onClose}
            footer={[
                <Button onClick={onClose}>Hủy</Button>,
                <Button danger onClick={onDelete}>
                    Xóa
                </Button>,
                <Button type='primary' onClick={onSubmit}>
                    Lưu
                </Button>,
            ]}
        >
            <Form layout='vertical' onFinish={onSubmit} form={form}>
                <Form.Item
                    name='title'
                    label='Tiêu đề'
                    rules={[
                        {
                            required: true,
                            message: 'Không được để trống',
                        },
                    ]}
                    initialValue={label.title}
                >
                    <Input
                        placeholder='Nhập tiêu đề của nhãn'
                        autoFocus={true}
                    />
                </Form.Item>
                <Form.Item label='Chọn màu sắc'>
                    <ColorPicker
                        value={colorRgb}
                        onChange={setColorRgb}
                        format={formatRgb}
                        onFormatChange={setFormatRgb}
                        presets={[
                            {
                                label: 'Gợi ý',
                                colors: listRecommendColor,
                            },
                        ]}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalUpdateLabel;
