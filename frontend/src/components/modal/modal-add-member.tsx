import { Form, Modal, Select } from 'antd';
import { DefaultOptionType } from 'antd/es/select';
import { useEffect, useState } from 'react';
import {
    addMemberRequest,
    getListAvailableMemberRequest,
} from 'src/apis/member.request';
import { CardInforType } from 'src/types/card';
import { ErrorType, ResponseType } from 'src/types/typing';
import {
    memberAdapterMemberOption,
    memberOptionAdapterMember,
} from 'src/utils/dtos/member.dto';
import openNotification from 'src/utils/openNotification';
type ModalAddMemberProps = {
    open: boolean;
    onClose: () => void;
    card: CardInforType;
};
function ModalAddMember({ open, onClose, card }: ModalAddMemberProps) {
    const [form] = Form.useForm();
    const [listAvailableMemberOption, setListAvailableMemberOption] =
        useState<DefaultOptionType[]>();
    const onSubmit = async () => {
        try {
            const listSelected = form.getFieldValue('members');
            const payload = memberOptionAdapterMember(listSelected, card.id);
            const result: ResponseType = (await addMemberRequest(payload)).data;
            if (result.status) {
                openNotification('Thành công', result.message);
                form.resetFields();
                onClose();
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    const getListAvailable = async () => {
        try {
            const result: ResponseType = (
                await getListAvailableMemberRequest(card)
            ).data;
            if (result.status) {
                const listOption = memberAdapterMemberOption(result.data);
                setListAvailableMemberOption(listOption);
            }
        } catch (error) {}
    };

    useEffect(() => {
        getListAvailable();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <Modal
            title='Thêm thành viên'
            open={open}
            onOk={onSubmit}
            onCancel={onClose}
            okText='Đồng ý'
            cancelText='Hủy bỏ'
        >
            <Form layout='vertical' onFinish={onSubmit} form={form}>
                <Form.Item name='members'>
                    <Select
                        mode='multiple'
                        allowClear
                        style={{ width: '100%' }}
                        placeholder='--Chọn thành viên--'
                        // onChange={handleChange}
                        filterOption={(input, option) => {
                            return String(option?.label)
                                .toLowerCase()
                                .includes(input.toLocaleLowerCase());
                        }}
                        options={listAvailableMemberOption}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalAddMember;
