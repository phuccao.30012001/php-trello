import { Form, Input, Modal, ColorPicker } from 'antd';
import { Color, ColorPickerProps } from 'antd/es/color-picker';
import { useState } from 'react';
import { addLabelRequest } from 'src/apis/label.request';
import { CardInforType } from 'src/types/card';
import { AddNewLabelType } from 'src/types/label';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
import { listRecommendColor } from '../constants';
type ModalAddLabelProps = {
    open: boolean;
    onClose: () => void;
    card: CardInforType;
};
function ModalAddLabel({ open, onClose, card }: ModalAddLabelProps) {
    const [form] = Form.useForm();

    const [colorRgb, setColorRgb] = useState<Color | string>(
        'rgb(22, 119, 255)'
    );
    const [formatRgb, setFormatRgb] =
        useState<ColorPickerProps['format']>('rgb');
    const onSubmit = async () => {
        try {
            const rgbString =
                typeof colorRgb === 'string'
                    ? colorRgb
                    : colorRgb.toRgbString();
            const payload: AddNewLabelType = {
                color: rgbString,
                card_id: Number(card.id),
                title: form.getFieldValue('title'),
            };
            const result: ResponseType = (await addLabelRequest(payload)).data;
            if (result.status) {
                form.resetFields();
                openNotification('Thành công', result.message);
                onClose();
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    return (
        <Modal
            title='Thêm nhãn'
            open={open}
            onOk={onSubmit}
            onCancel={onClose}
            okText='Đồng ý'
            cancelText='Hủy bỏ'
        >
            <Form layout='vertical' onFinish={onSubmit} form={form}>
                <Form.Item
                    name='title'
                    label='Tiêu đề'
                    rules={[
                        {
                            required: true,
                            message: 'Không được để trống',
                        },
                    ]}
                >
                    <Input
                        placeholder='Nhập tiêu đề của nhãn'
                        autoFocus={true}
                    />
                </Form.Item>
                <Form.Item label='Chọn màu sắc'>
                    <ColorPicker
                        value={colorRgb}
                        onChange={setColorRgb}
                        format={formatRgb}
                        onFormatChange={setFormatRgb}
                        presets={[
                            {
                                label: 'Gợi ý',
                                colors: listRecommendColor,
                            },
                        ]}
                    />
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalAddLabel;
