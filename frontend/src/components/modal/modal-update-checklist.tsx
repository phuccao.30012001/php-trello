import { Form, Input, Modal } from 'antd';
import { updateChecklistRequest } from 'src/apis/checklist.request';
import { ChecklistInforType } from 'src/types/checklist';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';

type ModalUpdateChecklistProps = {
    open: boolean;
    onClose: () => void;
    checklist: ChecklistInforType;
};

function ModalUpdateChecklist({
    open,
    onClose,
    checklist,
}: ModalUpdateChecklistProps) {
    const [form] = Form.useForm();
    const onSubmit = async () => {
        try {
            const payload: ChecklistInforType = {
                content: form.getFieldValue('content'),
                id: Number(checklist.id),
                status: Number(checklist.status),
                card_id: Number(checklist.card_id),
            };

            const result: ResponseType = (await updateChecklistRequest(payload))
                .data;
            if (result.status) {
                form.resetFields();
                openNotification('Thành công', result.message);
                onClose();
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };
    return (
        <Modal
            title='Chỉnh sửa công việc'
            open={open}
            onOk={onSubmit}
            onCancel={onClose}
            okText='Đồng ý'
            cancelText='Hủy bỏ'
        >
            <Form layout='vertical' form={form} onFinish={onSubmit}>
                <Form.Item name='content' initialValue={checklist.content}>
                    <Input placeholder='Tên công việc' />
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalUpdateChecklist;
