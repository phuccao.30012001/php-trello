import { Modal, Typography } from 'antd';
import { deleteColumnRequest } from 'src/apis/column.request';
import { ColumnInforType } from 'src/types/column';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
type ModalDeleteColumnProps = {
    open: boolean;
    onClose: () => void;
    column: ColumnInforType;
};
const { Text } = Typography;
function ModalDeleteColumn({ open, onClose, column }: ModalDeleteColumnProps) {
    const onSubmit = async () => {
        try {
            const result: ResponseType = (await deleteColumnRequest(column))
                .data;
            if (result.status) {
                openNotification('Thành công', result.message);
                onClose();
            } else {
                openNotification('Phản hồi', result.message);
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };
    return (
        <Modal
            title='Xóa cột'
            open={open}
            onOk={onSubmit}
            onCancel={onClose}
            okText='Đồng ý'
            cancelText='Hủy bỏ'
        >
            <Text>Bạn có chắc chắn muốn xóa cột này không?</Text>
        </Modal>
    );
}

export default ModalDeleteColumn;
