import { Form, Input, Modal } from 'antd';
import React, { useContext, useEffect, useState } from 'react';
import {
    addCommentRequest,
    getListCommentRequest,
} from 'src/apis/comment.request';
import { AppContext } from 'src/context/AppProvider';
import { CardInforType } from 'src/types/card';
import {
    AddCommentType,
    CommentInforType,
    FormAddType,
} from 'src/types/comment';
import { ErrorType, ResponseType } from 'src/types/typing';
import openNotification from 'src/utils/openNotification';
import CommentBox from '../manage/comment-box';

type ModalCommentProps = {
    open: boolean;
    onClose: () => void;
    card: CardInforType;
};
function ModalComment({ open, onClose, card }: ModalCommentProps) {
    const [form] = Form.useForm();
    const { user } = useContext(AppContext);
    const [listComment, setListComment] = useState<CommentInforType[]>();
    const onSubmit = async ({ content }: FormAddType) => {
        try {
            const payload: AddCommentType = {
                content,
                card_id: Number(card.id),
                user_id: Number(user?.id || 0),
            };
            const result: ResponseType = (await addCommentRequest(payload))
                .data;
            if (result.status) {
                form.resetFields();
                await getListComment();
            }
        } catch (error) {
            openNotification('Lỗi', (error as ErrorType).response.data.message);
        }
    };

    const getListComment = async () => {
        try {
            const result: ResponseType = (await getListCommentRequest(card))
                .data;
            if (result.status) {
                setListComment(result.data);
            }
        } catch (error) {}
    };

    useEffect(() => {
        getListComment();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [card]);

    useEffect(() => {
        document
            .getElementById('cmt-box')
            ?.scrollTo(
                0,
                document.getElementById('cmt-box')?.scrollHeight || 0
            );
    }, [listComment]);
    return (
        <Modal
            title='Bình luận'
            open={open}
            // onOk={onSubmit}
            onCancel={onClose}
            footer={[]}
        >
            {listComment && <CommentBox listComment={listComment} />}
            <Form layout='vertical' form={form} onFinish={onSubmit}>
                <Form.Item name='content'>
                    <Input placeholder='Nhập bình luận' autoFocus={true} />
                </Form.Item>
            </Form>
        </Modal>
    );
}

export default ModalComment;
