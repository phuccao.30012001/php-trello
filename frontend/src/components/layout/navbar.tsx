import { Avatar, Dropdown, Typography, MenuProps } from 'antd';
import style from 'src/styles/layout.module.scss';
import { UserOutlined } from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import { removeToken } from 'src/utils/storeToken';

function NavbarCustom() {
    const router = useNavigate();
    const onLogout = () => {
        removeToken();
        router('/login');
    };
    const subMenu: MenuProps['items'] = [
        {
            label: <Link to='/profile'>Thông tin</Link>,
            key: 0,
        },
        {
            label: <p onClick={onLogout}>Đăng xuất</p>,
            key: 1,
        },
    ];
    return (
        <div className={style['menu-container']}>
            <Link to='/manage'>
                <Typography.Title className={style['title-navbar']}>
                    Manage
                </Typography.Title>
            </Link>
            <Dropdown menu={{ items: subMenu }} trigger={['click']}>
                <Link
                    to=''
                    onClick={(e) => {
                        e.preventDefault();
                    }}
                >
                    <Avatar icon={<UserOutlined />} />
                </Link>
            </Dropdown>
        </div>
    );
}

export default NavbarCustom;
