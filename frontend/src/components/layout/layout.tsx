// import { ShoppingCartOutlined, ReadOutlined, ShopOutlined, AppstoreAddOutlined } from '@ant-design/icons';
import { Layout } from 'antd';
import { Header, Content } from 'antd/lib/layout/layout';
// import { useRouter } from 'next/router';
import React from 'react';
import style from 'src/styles/layout.module.scss';
import NavbarCustom from './navbar';
type LayoutProps = {
    children: React.ReactNode;
};

const LayoutCustom: React.FC<LayoutProps> = ({ children }) => {
    return (
        <Layout>
            <Header className={style['layout-header-container']}>
                <NavbarCustom />
            </Header>
            <Content className={style['layout-content-container']}>
                {children}
            </Content>
        </Layout>
    );
};

export default LayoutCustom;
